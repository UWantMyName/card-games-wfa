﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardGames
{
    public partial class BlackJack : Form
    {
        Deck deck = new Deck();
        int nrcards = 52;

        Card[] MyHand = new Card[12];
        int nrMyHand = 0, sumMyHand = 0;

        Card[] MyHand1 = new Card[12];
        int nrMyHand1 = 0, sumMyHand1 = 0;

        Card[] MyHand2 = new Card[12];
        int nrMyHand2 = 0, sumMyHand2 = 0;

        bool MyBJ = false, EnBJ = false;
        bool MyBusted = false, EnBusted = false;

        Card[] EnHand = new Card[12];
        int nrEnHand = 0, sumEnHand = 0;

        Point p1 = new Point();
        Point p2 = new Point();

        Card MCard1 = new Card();
        Card MCard2 = new Card();
        Card ECard1 = new Card();
        Card ECard2 = new Card();

        PictureBox[] M = new PictureBox[20];
        PictureBox[] M1 = new PictureBox[20];
        PictureBox[] M2 = new PictureBox[20];
        PictureBox[] E = new PictureBox[20];

        public int BetSum = 0;
        public int BankSum;
        bool splitted = false;
        bool firstHand = false;
        bool secondHand = false;

        public BlackJack()
        {
            InitializeComponent(); 
            if (!File.Exists("BankAmmountMoney.txt")) MessageBox.Show("You are missing a file. Maybe you've deleted it?");
            else
            {
                using (StreamReader read = new StreamReader("BankAmmountMoney.txt"))
                {
                    String line;

                    line = read.ReadLine();
                    BankSum = int.Parse(line);
                    BankSum_Label.Text = "In Bank: " + BankSum + "$";
                }
            }
            Start();
        }

        private void RePlay()
        {
            BlackJack f = new BlackJack();
            f.Show();
            this.Hide();
        }

        private void Create()
        {
            deck.GenerateCards();
            deck.ShuffleCards(52);

            MyHand[1] = deck.cards[nrcards]; nrcards--;
            //MCard1.symbol = "4"; MCard1.signcode = 3;
            MCard1 = MyHand[1];
            //MyHand[1] = MCard1;
            MCard1_Picture.Image = Image.FromFile(MyHand[1].GetNameOfFile());


            MyHand[2] = deck.cards[nrcards]; nrcards--;
            //MCard2.symbol = "4"; MCard1.signcode = 4;
            MCard2 = MyHand[2];
            //MyHand[2] = MCard2;
            MCard2_Picture.Image = Image.FromFile(MyHand[2].GetNameOfFile());
            nrMyHand = 2; nrEnHand = 2;

            EnHand[1] = deck.cards[nrcards]; nrcards--;
            ECard1 = EnHand[1];
            ECard1_Picture.Image = Image.FromFile(EnHand[1].GetNameOfFile());
            EnHand[2] = deck.cards[nrcards]; nrcards--;
            ECard2 = EnHand[2];
            //ECard2_Picture.Image = Image.FromFile(EnHand[2].GetNameOfFile());

            MCard1_Picture.Show();
            MCard2_Picture.Show();
            p1 = MCard2_Picture.Location;
            ECard1_Picture.Show();
            ECard2_Picture.Show();
            p2 = ECard2_Picture.Location;

            sumMyHand = GiveSum(MyHand, nrMyHand);
            if (sumMyHand == 21) MyBJ = true;
            sumEnHand = GiveSum(EnHand, nrEnHand);
            if (sumEnHand == 21) EnBJ = true;
        }

        private void Start()
        {
            ClearBet_Button.Show();
            BetSum_Label.Show();
            BankSum_Label.Show();
            PlayGame_Button.Show();
        }

        private void PlayGame_Button_Click(object sender, EventArgs e)
        {
            BetSum = int.Parse(BetSum_TextBox.Text);

            if (!(2 <= BetSum && BetSum <= 500)) MessageBox.Show("Make the bet again.");
            else
            {
                PlayGame_Button.Hide();
                Create();
                BetSum_Label.Text = "Bet Sum: " + BetSum.ToString() + "$";
                Stand_Button.Show();
                BetPlacing_Text.Hide();
                BetSum_TextBox.Hide();
                Hit_Button.Show();
                Double_Button.Show();
                if (MyHand[1].symbol == MyHand[2].symbol) Split_Button.Show();
                BlackJackCheck();
            }
        }

        private void BlackJackCheck()
        {
            if (MyBJ && !EnBJ)
            {
                ECard2_Picture.Image = Image.FromFile(ECard2.GetNameOfFile());
                MessageBox.Show("You have a Black Jack!");
                BetSum = BetSum + BetSum / 2;
                BankSum += BetSum;
                WriteIn();
                Stand_Button.Hide();
                Hit_Button.Hide();
                Double_Button.Hide();
                Split_Button.Hide();
                RePlay();
            }
            if (!MyBJ && EnBJ)
            {
                ECard2_Picture.Image = Image.FromFile(ECard2.GetNameOfFile());
                MessageBox.Show("The dealer's got a Black Jack!");
                BetSum = BetSum + BetSum / 2;
                BankSum -= BetSum;
                WriteIn();
                RePlay();
                Stand_Button.Hide();
                Hit_Button.Hide();
                Double_Button.Hide();
                Split_Button.Hide();
            }
            if (MyBJ && EnBJ)
            {
                ECard2_Picture.Image = Image.FromFile(ECard2.GetNameOfFile());
                MessageBox.Show("Both of you got a Black Jack. Draw.");
                Stand_Button.Hide();
                Hit_Button.Hide();
                Double_Button.Hide();
                Split_Button.Hide();
                RePlay();
            }
        }

        private int GiveSum(Card[] Hand, int nrHand)
        {
            int sum = 0, i;
            int nrA = 0;

            for (i = 1; i <= nrHand; i++)
                if (Hand[i].symbol == "A")
                {
                    sum += 11;
                    nrA++;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
                else if (Hand[i].symbol == "2")
                {
                    sum += 2;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
                else if (Hand[i].symbol == "3")
                {
                    sum += 3;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
                else if (Hand[i].symbol == "4")
                {
                    sum += 4;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
                else if (Hand[i].symbol == "5")
                {
                    sum += 5;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
                else if (Hand[i].symbol == "6")
                {
                    sum += 6;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
                else if (Hand[i].symbol == "7")
                {
                    sum += 7;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
                else if (Hand[i].symbol == "8")
                {
                    sum += 8;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
                else if (Hand[i].symbol == "9")
                {
                    sum += 9;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
                else if (Hand[i].symbol == "10" || Hand[i].symbol == "J" || Hand[i].symbol == "Q" || Hand[i].symbol == "K")
                {
                    sum += 10;
                    if (sum > 21 && nrA != 0) { sum -= 10; nrA--; }
                }
            return sum;
        }

        private void Quit_Button_Click(object sender, EventArgs e)
        {
            MainMenuForm f = new MainMenuForm();
            f.Show();
            this.Close();
        }

        private void ClearBet_Button_Click(object sender, EventArgs e)
        {
            BetSum = 0;
            BetSum_Label.Text = "Bet Sum: 0$";
        }

        private void WriteIn()
        {
            using (StreamWriter write = new StreamWriter("BankAmmountMoney.txt"))
            {
                write.WriteLine(BankSum);
            }
        }

        private void CreateFirstHand()
        {
            firstHand = true;
            nrMyHand1 = 2;
            MyHand1[1] = MCard1;
            MyHand1[2] = deck.cards[nrcards]; nrcards--;
            MCard2_Picture.Image = Image.FromFile(MyHand1[2].GetNameOfFile());
            sumMyHand1 = GiveSum(MyHand1,nrMyHand1);
        }

        private void CreateSecondHand()
        {
            int i;

            Double_Button.Show();
            Stand_Button.Show();
            Hit_Button.Show();

            for (i = 3; i <= nrMyHand1; i++)
                M1[i].Hide();
                
            secondHand = true;
            p1.X = MCard2_Picture.Location.X;
            p1.Y = MCard2_Picture.Location.Y;
            nrMyHand2 = 2;
            MyHand2[1] = MCard2;
            MyHand2[2] = deck.cards[nrcards]; nrcards--;
            MCard1_Picture.Image = Image.FromFile(MyHand2[1].GetNameOfFile());
            MCard2_Picture.Image = Image.FromFile(MyHand2[2].GetNameOfFile());
            sumMyHand2 = GiveSum(MyHand2, nrMyHand2);
        }

        private void Split_Button_Click(object sender, EventArgs e)
        {
            splitted = true;
            CreateFirstHand();
            Split_Button.Hide();

            MyHand2[1] = MCard2;
            MyHand2[2] = deck.cards[nrcards]; nrcards--;
        }

        private void EnemyTurn()
        {
            ECard2_Picture.Image = Image.FromFile(EnHand[nrEnHand].GetNameOfFile());

            while (sumEnHand < 17)
            {
                nrEnHand++;
                EnHand[nrEnHand] = deck.cards[nrcards];
                nrcards--;

                PictureBox ECard_Picture = new PictureBox();

                ECard_Picture.Image = Image.FromFile(EnHand[nrEnHand].GetNameOfFile());
                ECard_Picture.SizeMode = PictureBoxSizeMode.StretchImage;
                ECard_Picture.Size = new Size() { Width = 125, Height = 180 };
                ECard_Picture.Name = "ECard" + nrMyHand;
                p2.X += 34; p2.Y -= 3;
                ECard_Picture.Location = p2;
                ECard_Picture.BackColor = Color.Transparent;
                this.Controls.Add(ECard_Picture);
                ECard_Picture.BringToFront();

                sumEnHand = GiveSum(EnHand, nrEnHand);
            }

            if (!splitted)
            {
                if (sumEnHand > 21) { MessageBox.Show("Dealer's busted! You won!"); EnBusted = true; BankSum += BetSum; WriteIn(); RePlay(); }

                if (!MyBusted && !EnBusted)
                {
                    if (sumMyHand == sumEnHand) { MessageBox.Show("Draw."); }
                    if (sumMyHand < sumEnHand) { MessageBox.Show("You lost!"); BankSum -= BetSum; WriteIn(); }
                    if (sumMyHand > sumEnHand) { MessageBox.Show("You won!"); BankSum += BetSum; WriteIn(); }
                    RePlay();    
                }
            }
            else
            {
                if (!EnBusted)
                {
                    int sum = 0;

                    if (sumMyHand1 > 21) BankSum -= BetSum;
                    else { BankSum += BetSum; sum += BetSum; }

                    if (sumMyHand2 > 21) BankSum -= BetSum;
                    else { BankSum += BetSum; sum += BetSum; }

                    MessageBox.Show("You won " + sum.ToString() + " $ in this match.");
                    RePlay();
                }
                else
                {
                    int sum = 0;

                    if (sumMyHand1 < sumEnHand) BankSum -= BetSum;
                    else if (sumMyHand1 > sumEnHand && sumMyHand1 <= 21) { BankSum += BetSum; sum += BetSum; }

                    if (sumMyHand2 < sumEnHand) BankSum -= BetSum;
                    else if (sumMyHand2 > sumEnHand && sumMyHand2 <= 21) { BankSum += BetSum; sum += BetSum; }

                    MessageBox.Show("You won " + sum.ToString() + " $ in this match.");
                    RePlay();
                }
            }
        }

        private void Stand_Button_Click(object sender, EventArgs e)
        {
            if (!firstHand && !secondHand)
            {
                Stand_Button.Hide();
                Hit_Button.Hide();
                Double_Button.Hide();
                Split_Button.Hide();

                EnemyTurn();
            }
            else if (firstHand)
            {
                firstHand = false;
                CreateSecondHand();
            }
            else if (secondHand)
            {
                secondHand = false;
                Stand_Button.Hide();
                Hit_Button.Hide();
                Double_Button.Hide();
                Split_Button.Hide();

                EnemyTurn();
            }
        }

        private void Double_Button_Click(object sender, EventArgs e)
        {
            if (BetSum * 2 > BankSum) { MessageBox.Show("You can't double your money. You don't have enough in your bank."); }
            else
            {
                if (!firstHand && !secondHand)
                {
                    Stand_Button.Hide();
                    Hit_Button.Hide();
                    Double_Button.Hide();
                    Split_Button.Hide();

                    nrMyHand++;
                    MyHand[nrMyHand] = deck.cards[nrcards];
                    nrcards--;

                    PictureBox MCard_Picture = new PictureBox();

                    MCard_Picture.Image = Image.FromFile(MyHand[nrMyHand].GetNameOfFile());
                    MCard_Picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    MCard_Picture.Size = new Size() { Width = 125, Height = 180 };
                    MCard_Picture.Name = "MCard" + nrMyHand;
                    p1.X += 57; p1.Y -= 3;
                    MCard_Picture.Location = p1;
                    MCard_Picture.BackColor = Color.Transparent;
                    this.Controls.Add(MCard_Picture);
                    MCard_Picture.BringToFront();

                    sumMyHand = GiveSum(MyHand, nrMyHand);
                    BetSum *= 2;
                    BetSum_Label.Text = "Bet Sum: " + BetSum;
                    if (sumMyHand > 21) { MessageBox.Show("You're busted!! You lost!"); MyBusted = true; WriteIn(); RePlay(); }
                    else EnemyTurn();
                }
                else if (firstHand)
                {
                    nrMyHand1++;
                    MyHand1[nrMyHand1] = deck.cards[nrcards];
                    nrcards--;

                    PictureBox MCard_Picture = new PictureBox();

                    MCard_Picture.Image = Image.FromFile(MyHand1[nrMyHand1].GetNameOfFile());
                    MCard_Picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    MCard_Picture.Size = new Size() { Width = 125, Height = 180 };
                    MCard_Picture.Name = "MCard" + nrMyHand1;
                    p1.X += 34; p1.Y -= 3;
                    MCard_Picture.Location = p1;
                    MCard_Picture.BackColor = Color.Transparent;
                    this.Controls.Add(MCard_Picture);
                    MCard_Picture.BringToFront();

                    sumMyHand1 = GiveSum(MyHand1, nrMyHand1);
                    BetSum *= 2;
                    BetSum_Label.Text = "Bet Sum: " + BetSum;
                    if (sumMyHand1 > 21) { MessageBox.Show("You're busted!!"); MyBusted = true; WriteIn(); RePlay(); }
                    firstHand = false;
                    CreateSecondHand();
                }
                else if (secondHand)
                {
                    nrMyHand2++;
                    MyHand2[nrMyHand2] = deck.cards[nrcards];
                    nrcards--;

                    PictureBox MCard_Picture = new PictureBox();

                    MCard_Picture.Image = Image.FromFile(MyHand2[nrMyHand2].GetNameOfFile());
                    MCard_Picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    MCard_Picture.Size = new Size() { Width = 125, Height = 180 };
                    MCard_Picture.Name = "MCard" + nrMyHand2;
                    p1.X += 34; p1.Y -= 3;
                    MCard_Picture.Location = p1;
                    MCard_Picture.BackColor = Color.Transparent;
                    this.Controls.Add(MCard_Picture);
                    MCard_Picture.BringToFront();

                    sumMyHand2 = GiveSum(MyHand2, nrMyHand2);
                    BetSum *= 2;
                    BetSum_Label.Text = "Bet Sum: " + BetSum;
                    if (sumMyHand2 > 21) { MessageBox.Show("You're busted!!"); MyBusted = true; WriteIn(); RePlay(); }
                    secondHand = false;
                    EnemyTurn();
                }
            }
        }

        private void Hit_Button_Click(object sender, EventArgs e)
        {
            if (!firstHand && !secondHand)
            {
                nrMyHand++;
                MyHand[nrMyHand] = deck.cards[nrcards];
                nrcards--;

                M[nrMyHand] = new PictureBox();
                M[nrMyHand].Image = Image.FromFile(MyHand[nrMyHand].GetNameOfFile());
                M[nrMyHand].SizeMode = PictureBoxSizeMode.StretchImage;
                M[nrMyHand].Size = new Size() { Width = 125, Height = 180 };
                M[nrMyHand].Name = "MCard" + nrMyHand;
                p1.X += 34; p1.Y -= 3;
                M[nrMyHand].Location = p1;
                M[nrMyHand].BackColor = Color.Transparent;
                this.Controls.Add(M[nrMyHand]);
                M[nrMyHand].BringToFront();

                sumMyHand = GiveSum(MyHand, nrMyHand);
                if (sumMyHand == 21)
                {
                    Stand_Button.Hide();
                    Hit_Button.Hide();
                    Double_Button.Hide();
                    Split_Button.Hide();
                    EnemyTurn();
                }
                else if (sumMyHand > 21)
                {
                    Stand_Button.Hide();
                    Hit_Button.Hide();
                    Double_Button.Hide();
                    Split_Button.Hide();
                    MessageBox.Show("You're busted!! You lost!");
                    RePlay();
                    BankSum -= BetSum;
                    WriteIn();
                    MyBusted = true;
                }
            }
            else if (firstHand)
            {
                nrMyHand1++;
                MyHand1[nrMyHand1] = deck.cards[nrcards];
                nrcards--;

                M1[nrMyHand1] = new PictureBox();
                M1[nrMyHand1].Image = Image.FromFile(MyHand1[nrMyHand1].GetNameOfFile());
                M1[nrMyHand1].SizeMode = PictureBoxSizeMode.StretchImage;
                M1[nrMyHand1].Size = new Size() { Width = 125, Height = 180 };
                M1[nrMyHand1].Name = "MCard" + nrMyHand1;
                p1.X += 34; p1.Y -= 3;
                M1[nrMyHand1].Location = p1;
                M1[nrMyHand1].BackColor = Color.Transparent;
                this.Controls.Add(M1[nrMyHand1]);
                M1[nrMyHand1].BringToFront();

                sumMyHand1 = GiveSum(MyHand1, nrMyHand1);
                if (sumMyHand1 == 21)
                {
                    Stand_Button.Hide();
                    Hit_Button.Hide();
                    Double_Button.Hide();
                    Split_Button.Hide();

                    firstHand = false;
                    CreateSecondHand();
                }
                else if (sumMyHand1 > 21)
                {
                    Stand_Button.Hide();
                    Hit_Button.Hide();
                    Double_Button.Hide();
                    Split_Button.Hide();
                    MessageBox.Show("You're busted!!");
                    RePlay();
                    firstHand = false;
                    CreateSecondHand();
                    BankSum -= BetSum;
                    WriteIn();
                    MyBusted = true;
                }
            }
            else if (secondHand)
            {
                nrMyHand2++;
                MyHand2[nrMyHand2] = deck.cards[nrcards];
                nrcards--;

                M2[nrMyHand2] = new PictureBox();
                M2[nrMyHand2].Image = Image.FromFile(MyHand2[nrMyHand2].GetNameOfFile());
                M2[nrMyHand2].SizeMode = PictureBoxSizeMode.StretchImage;
                M2[nrMyHand2].Size = new Size() { Width = 125, Height = 180 };
                M2[nrMyHand2].Name = "MCard" + nrMyHand2;
                p1.X += 34; p1.Y -= 3;
                M2[nrMyHand2].Location = p1;
                M2[nrMyHand2].BackColor = Color.Transparent;
                this.Controls.Add(M2[nrMyHand2]);
                M2[nrMyHand2].BringToFront();

                sumMyHand2 = GiveSum(MyHand2, nrMyHand2);
                if (sumMyHand2 == 21)
                {
                    Stand_Button.Hide();
                    Hit_Button.Hide();
                    Double_Button.Hide();
                    Split_Button.Hide();
                    secondHand = false;
                    EnemyTurn();
                }
                else if (sumMyHand2 > 21)
                {
                    Stand_Button.Hide();
                    Hit_Button.Hide();
                    Double_Button.Hide();
                    Split_Button.Hide();
                    MessageBox.Show("You're busted!!");
                    RePlay();
                    secondHand = false;
                    BankSum -= BetSum;
                    WriteIn();
                    MyBusted = true;
                    EnemyTurn();
                }
            }
        }
    }
}
