﻿namespace CardGames
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenuForm));
            this.label1 = new System.Windows.Forms.Label();
            this.Play_Label = new System.Windows.Forms.Label();
            this.Exit_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Sitka Display", 50F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(111, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(364, 97);
            this.label1.TabIndex = 0;
            this.label1.Text = "Card Games";
            // 
            // Play_Label
            // 
            this.Play_Label.AutoSize = true;
            this.Play_Label.BackColor = System.Drawing.Color.Transparent;
            this.Play_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Play_Label.Font = new System.Drawing.Font("Sitka Display", 35F);
            this.Play_Label.ForeColor = System.Drawing.Color.White;
            this.Play_Label.Location = new System.Drawing.Point(71, 132);
            this.Play_Label.Name = "Play_Label";
            this.Play_Label.Size = new System.Drawing.Size(107, 68);
            this.Play_Label.TabIndex = 1;
            this.Play_Label.Text = "Play";
            this.Play_Label.Click += new System.EventHandler(this.Play_Label_Click);
            this.Play_Label.MouseEnter += new System.EventHandler(this.PlayLabel_MouseEnter);
            this.Play_Label.MouseLeave += new System.EventHandler(this.PlayLabel_MouseLeave);
            // 
            // Exit_Label
            // 
            this.Exit_Label.AutoSize = true;
            this.Exit_Label.BackColor = System.Drawing.Color.Transparent;
            this.Exit_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit_Label.Font = new System.Drawing.Font("Sitka Display", 35F);
            this.Exit_Label.ForeColor = System.Drawing.Color.White;
            this.Exit_Label.Location = new System.Drawing.Point(75, 320);
            this.Exit_Label.Name = "Exit_Label";
            this.Exit_Label.Size = new System.Drawing.Size(103, 68);
            this.Exit_Label.TabIndex = 2;
            this.Exit_Label.Text = "Exit";
            this.Exit_Label.Click += new System.EventHandler(this.ExitLabel_Click);
            this.Exit_Label.MouseLeave += new System.EventHandler(this.ExitLabel_MouseLeave);
            this.Exit_Label.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ExitLabel_MouseMove);
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(589, 457);
            this.Controls.Add(this.Exit_Label);
            this.Controls.Add(this.Play_Label);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainMenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Play_Label;
        private System.Windows.Forms.Label Exit_Label;
    }
}

