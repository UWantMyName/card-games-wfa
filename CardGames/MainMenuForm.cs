﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardGames
{
    public partial class MainMenuForm : Form
    {
        public MainMenuForm()
        {
            InitializeComponent();
        }

        private void PlayLabel_MouseLeave(object sender, EventArgs e)
        {
            Play_Label.Font = new Font("Sitka Display", 35);
        }

        private void ExitLabel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ExitLabel_MouseLeave(object sender, EventArgs e)
        {
            Exit_Label.Font = new Font("Sitka Display", 35);
        }

        private void ExitLabel_MouseMove(object sender, MouseEventArgs e)
        {
            Exit_Label.Font = new Font("Sitka Display", 35, FontStyle.Bold);
        }

        private void PlayLabel_MouseEnter(object sender, EventArgs e)
        {
            Play_Label.Font = new Font("Sitka Display", 35, FontStyle.Bold);
        }

        private void Play_Label_Click(object sender, EventArgs e)
        {
            TypesOfGames f = new TypesOfGames();

            f.Show();
            this.Hide();
        }
    }
}
