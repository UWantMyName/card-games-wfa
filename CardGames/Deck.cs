﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGames
{
    public class Deck
    {
        public Card[] cards = new Card[53];

        public void ShuffleCards(int end)
        {
            int i, j;
            Card aux = new Card();
            Random r = new Random();

            for (i = 1; i <= end; i++)
                for (j = 1; j <= end; j++)
                {
                    int val = r.Next(100);

                    if (val % 2 == 0 && i != j)
                    {
                        aux = cards[i];
                        cards[i] = cards[j];
                        cards[j] = aux;
                    }
                }

            for (i = 1; i <= end; i++)
                for (j = 1; j <= end; j++)
                {
                    int val = r.Next(100);

                    if (val % 2 == 0 && i != j)
                    {
                        aux = cards[i];
                        cards[i] = cards[j];
                        cards[j] = aux;
                    }
                }

            for (i = 1; i <= end; i++)
                for (j = 1; j <= end; j++)
                {
                    int val = r.Next(100);

                    if (val % 2 == 0 && i != j)
                    {
                        aux = cards[i];
                        cards[i] = cards[j];
                        cards[j] = aux;
                    }
                }
        }

        public void RegenerateCards(Card[] MyHand, int MyHandNumber, Card[] EnemyHand, int EnemyHandNumber)
        {
            bool[] avcards = new bool[53];
            int i, j;          

            for (i = 1; i <= 52; i++)
                avcards[i] = true;
            this.GenerateCards();

            // Tag the cards I have in my hand with false
            for (i = 1; i <= MyHandNumber; i++)
            {
                bool found = false;

                for (j = 1; j <= 52 && !found; j++)
                    if (MyHand[i] == this.cards[j]) { avcards[j] = false; found = true; }
            }

            // Tag the cards I have in enemy's hand with false
            for (i = 1; i <= MyHandNumber; i++)
            {
                bool found = false;

                for (j = 1; j <= 52 && !found; j++)
                    if (EnemyHand[i] == this.cards[j]) { avcards[j] = false; found = true; }
            }

            // I remake the deck using the cards with "true" tag
            int end = 52;

            for (i = 1; i <= end; i++)
                if (!avcards[i]) EliminateCard(i, end);
            this.ShuffleCards(end);
        }

        public void EliminateCard(int poz, int end)
        {
            int i;

            for (i = poz; i < end; i++)
                this.cards[i] = this.cards[i + 1];
            end--;
        }

        public void GenerateCards()
        {
            int i;

            for (i = 1; i <= 52; i++)
                cards[i] = new Card();

            cards[1].symbol = cards[2].symbol = cards[3].symbol = cards[4].symbol = "A";
            cards[5].symbol = cards[6].symbol = cards[7].symbol = cards[8].symbol = "2";
            cards[9].symbol = cards[10].symbol = cards[11].symbol = cards[12].symbol = "3";
            cards[13].symbol = cards[14].symbol = cards[15].symbol = cards[16].symbol = "4";
            cards[17].symbol = cards[18].symbol = cards[19].symbol = cards[20].symbol = "5";
            cards[21].symbol = cards[22].symbol = cards[23].symbol = cards[24].symbol = "6";
            cards[25].symbol = cards[26].symbol = cards[27].symbol = cards[28].symbol = "7";
            cards[29].symbol = cards[30].symbol = cards[31].symbol = cards[32].symbol = "8";
            cards[33].symbol = cards[34].symbol = cards[35].symbol = cards[36].symbol = "9";
            cards[37].symbol = cards[38].symbol = cards[39].symbol = cards[40].symbol = "10";
            cards[41].symbol = cards[42].symbol = cards[43].symbol = cards[44].symbol = "J";
            cards[45].symbol = cards[46].symbol = cards[47].symbol = cards[48].symbol = "Q";
            cards[49].symbol = cards[50].symbol = cards[51].symbol = cards[52].symbol = "K";

            for (i = 0; i <= 48; i+=4)
            {
                cards[i + 1].signcode = 3;
                cards[i + 2].signcode = 4;
                cards[i + 3].signcode = 5;
                cards[i + 4].signcode = 6;
            }

            // initialise the compatible cards with each card in the deck
            for (i = 1; i <= 52; i++)
                cards[i].CompatibilityCards();
        }
    }
}
