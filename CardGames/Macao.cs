﻿using System.Drawing.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CardGames
{
    public partial class Macao : Form
    {
        public int NrCards;
        public Deck deck = new Deck(); // there can be a maximum of 5 cards, and a player can hold a maximum of 25 cards
        public int nrcards = 52;
        public Card topcard = new Card();
        public Card[] MyHand = new Card[55]; // my hand
        public int MyHandNumber;         // the number of cards in my hand
        public Card[] EnemyHand = new Card[55]; // the enemy's hand
        public int EnemyHandNumber;         // the number of cards in my enemy's hand
        public bool Turn = true; // true = my turn, false = his turn
        public int nrTurns = 1;

        public int CardsAttack;
        public int TurnsAttack;
        public int ChangeSigncode;
        public Card[] QueueCard = new Card[10];
        public int nrCardsQueue;
        public bool Standing = false;
        public bool ace = false;
        public bool EnemyStanding = false;

        Timer EnemyWait = new Timer();
        int sec = 0;
        bool loadEnemyWait = false;

        public Macao()
        {
            InitializeComponent();
            deck.GenerateCards();
            deck.ShuffleCards(52);
            InitializeGame();
            //CardsAttack = 2;
            //TurnsAttack = 2;
            //ChangeSignCode_Panel.Show();
            //AceSignCode.Show();
            //EndTurn_Button.Show();
            //DrawAttackCards_Button.Show();
            //PlayCards_Button.Show();
            //QueueCards_Label.Show();
            //InsertQueue_Button.Show();
            //DeleteLastCard_Button.Show();
            //StandTurn_Button.Show();
            //CommandPanel.Show();
            PlayGame();
        }

        private void InitializeGame()
        {
            int j;

            // My Hand
            for (j = 1; j <= 5; j++)
            {
                MyHand[j] = deck.cards[nrcards];
                nrcards--;
            }
            MyHandNumber = 5;
            MyCards_ComboBox.Items.Add(MyHand[1].GetNameOfCard(MyHand[1]));
            MyCards_ComboBox.Items.Add(MyHand[2].GetNameOfCard(MyHand[2]));
            MyCards_ComboBox.Items.Add(MyHand[3].GetNameOfCard(MyHand[3]));
            MyCards_ComboBox.Items.Add(MyHand[4].GetNameOfCard(MyHand[4]));
            MyCards_ComboBox.Items.Add(MyHand[5].GetNameOfCard(MyHand[5]));
            // Enemy Hand

            for (j = 1; j <= MyHandNumber; j++)
            {
                EnemyHand[j] = deck.cards[nrcards];
                nrcards--;
            }
            EnemyHandNumber = 5;

            // The Card on top of the play
            string sym;
            topcard = deck.cards[nrcards];
            nrcards--;
            sym = topcard.symbol;
            if (sym == "2" || sym == "3" || sym == "4" || sym == "A")
            {
                int k;
                // That means that I have to change it with a neutral card
                Random crd = new Random();
                Card aux = new Card();
                int val = crd.Next(10);

                for (k = nrcards; val > 0; k--)
                    if (deck.cards[k].symbol != "2" && deck.cards[k].symbol != "3" && deck.cards[k].symbol != "4" && deck.cards[k].symbol != "A")
                        val--;
                aux = topcard;
                topcard = deck.cards[k];
                deck.cards[k] = aux;
            }
            TopCard.Image = Image.FromFile(topcard.GetNameOfFile());

            // Other stuff
            CardsLeft_Label.Text += nrcards;
            EnemyMessage_Label.Hide();
        }

        private bool Compatible()
        {
            int i;

            for (i = 1; i <= MyHandNumber; i++)
                if (MyHand[i].symbol == topcard.symbol || MyHand[i].signcode == topcard.signcode) return true;
            return false;
        }

        private bool CanCounterEAttack()
        {
            int i;

            for (i = 1; i <= EnemyHandNumber; i++)
                if (topcard.symbol == "2" && EnemyHand[i].symbol == "2") return true;
                else if (topcard.symbol == "3" && EnemyHand[i].symbol == "3") return true;
                else if (EnemyHand[i].symbol == "7") return true;

            return false;
        }

        private bool CanCounterAttack()
        {
            int i;

            for (i = 1; i <= MyHandNumber; i++)
                if (topcard.symbol == "2" && MyHand[i].symbol == "2") return true;
                else if (topcard.symbol == "3" && MyHand[i].symbol == "3") return true;
                else if (MyHand[i].symbol == "7") return true;
            /*
            if (topcard.symbol == "2" && (MyHand[i].symbol == "2" || MyHand[i].symbol == "7")) return true;
            else if (topcard.symbol == "3" && (MyHand[i].symbol == "3" || MyHand[i].symbol == "7")) return true;
            */

            return false;
        }

        private bool CanCounterTurnsAttack()
        {
            int i;

            for (i = 1; i <= MyHandNumber; i++)
                if (MyHand[i].symbol == "7" || MyHand[i].symbol == "4") return true;

            return false;
        }

        private void PlayGame()
        {
            QueueCards_Label.Text = "Play these cards:\n";
            InsertQueue_Button.Show();
            DeleteLastCard_Button.Show();
            QueueCards_Label.Show();
            if (Turn)
            {
                CommandPanel.Show();
                EndTurn_Button.Show();
                PlayCards_Button.Show();
                if (MyHandNumber == 0)
                {
                    MessageBox.Show("You won!");
                    CommandPanel.Hide();
                    return;
                }
                if (EnemyHandNumber == 0)
                {
                    MessageBox.Show("You lost!");
                    CommandPanel.Hide();
                    return;
                }
                EnemyNrCards_Label.Text = "The enemy has " + EnemyHandNumber + " cards in his hand.";

                // First, let's see if I've been attacked with 2s and 3s
                if (CardsAttack > 0 && TurnsAttack == 0)
                {
                    bool canCounterAttack;

                    DrawAttackCards_Button.Text = "Draw " + CardsAttack + " Cards";
                    DrawAttackCards_Button.Show();
                    EndTurn_Button.Hide();
                    PlayCards_Button.Show();
                    InsertQueue_Button.Show();
                    DeleteLastCard_Button.Show();
                    nrCardsQueue = 0;

                    canCounterAttack = CanCounterAttack();

                    if (!canCounterAttack)
                    {
                        PlayCards_Button.Hide();
                        StandTurn_Button.Hide();
                        InsertQueue_Button.Hide();
                        DeleteLastCard_Button.Hide();
                        QueueCards_Label.Hide();
                        ChangeSignCode_Panel.Hide();
                    }
                    else
                    {
                        InsertQueue_Button.Show();
                        DeleteLastCard_Button.Show();
                        PlayCards_Button.Show();
                        QueueCards_Label.Show();
                    }
                }

                // Second, let's see if the opponent gave me a 4
                if (TurnsAttack > 0 && !EnemyStanding)
                {
                    if (!Standing)
                    {
                        bool canCounterTurnsAttack;

                        StandTurn_Button.Text = "Stand " + TurnsAttack + " Turns";
                        StandTurn_Button.Show();

                        canCounterTurnsAttack = CanCounterTurnsAttack();
                        if (canCounterTurnsAttack)
                        {
                            PlayCards_Button.Show();
                            InsertQueue_Button.Show();
                            DeleteLastCard_Button.Show();
                            EndTurn_Button.Hide();
                            nrCardsQueue = 0;
                        }
                        else
                        {
                            PlayCards_Button.Hide();
                            DrawAttackCards_Button.Hide();
                            EndTurn_Button.Hide();
                            InsertQueue_Button.Hide();
                            DeleteLastCard_Button.Hide();
                            QueueCards_Label.Hide();
                        }
                    }
                    else
                    {
                        if (TurnsAttack >= 1)
                        {
                            CommandPanel.Hide();
                            MessageBox.Show("You have " + (TurnsAttack - 1).ToString() + " turns left to stay.");
                            TurnsAttack--;
                            if (TurnsAttack <= 0) Standing = false;
                            SwitchTurns();
                            PlayGame();
                        }
                        else if (TurnsAttack <= 0)
                        {
                            Standing = false;
                            EndTurn_Button.Show();
                            PlayCards_Button.Show();
                            InsertQueue_Button.Show();
                            DeleteLastCard_Button.Show();
                            QueueCards_Label.Show();
                        }
                    }
                }

                if (TurnsAttack > 0 && EnemyStanding)
                {
                    StandTurn_Button.Hide();
                    EndTurn_Button.Show();
                    PlayCards_Button.Show();
                    InsertQueue_Button.Show();
                    DeleteLastCard_Button.Show();
                    QueueCards_Label.Show();
                }

                if (CardsAttack == 0 && TurnsAttack == 0)
                {
                    DrawAttackCards_Button.Hide();
                    EndTurn_Button.Show();
                    PlayCards_Button.Show();
                    InsertQueue_Button.Show();
                    DeleteLastCard_Button.Show();
                    QueueCards_Label.Show();
                    nrCardsQueue = 0;
                }   
            }
            else
            {
                if (MyHandNumber == 0)
                {
                    MessageBox.Show("You won!");
                    CommandPanel.Hide();
                    return;
                }
                if (EnemyHandNumber == 0)
                {
                    MessageBox.Show("You lost!");
                    CommandPanel.Hide();
                    return;
                }

                EnemyNrCards_Label.Text = "The enemy has " + EnemyHandNumber + " cards in his hand.";
                CommandPanel.Hide();
                EnemyMessage_Label.Show();
                // Check if he's been attacked
                if (CardsAttack > 0 && TurnsAttack == 0)
                {
                    bool canCA;

                    canCA = CanCounterEAttack();
                    if (canCA)
                    {
                        bool have = false;
                        bool have2 = false;
                        bool have3 = false;
                        bool have7 = false;
                        int k;

                        for (k = 1; k <= EnemyHandNumber; k++)
                            if (EnemyHand[k].symbol == "2" && topcard.symbol == "2") { have = true; have2 = true; }
                            else if (EnemyHand[k].symbol == "3" && topcard.symbol == "3") { have = true; have3 = true; }
                            else if (EnemyHand[k].symbol == "7" && (topcard.symbol == "2" || topcard.symbol == "3")) have7 = true;

                        if (!have && have7)
                        {
                            // I have a 50% chance to respond
                            Random seed = new Random();
                            int choice = seed.Next(100);

                            if (choice % 2 == 0)
                            {
                                // I'll play the 7, but I first have to find it so I can get rid of it
                                int poz;
                                bool found = false;

                                for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                    if (EnemyHand[poz].symbol == "7") found = true;

                                poz--;
                                topcard = EnemyHand[poz];
                                TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                CardsAttack = 0;
                                MessageBox.Show("The enemy counterred the attack with 7.");
                                EliminateCardFromList(poz);
                                EnemyMessage_Label.Hide();
                                SwitchTurns();
                                PlayGame();
                            }
                            else
                            {
                                // I'll have to take the cards
                                MessageBox.Show("The enemy draw " + CardsAttack + " cards.");
                                while (CardsAttack > 0)
                                {
                                    DrawCard(Turn);
                                    CardsAttack--;
                                }
                                CardsLeft_Label.Text = "Cards Left: " + nrcards;
                                SwitchTurns();
                                PlayGame();
                            }
                        }
                        else if (have && !have7)
                        {
                            // I'll have a 50% chance to respond
                            Random seed = new Random();
                            int choice = seed.Next(100);

                            if (choice % 2 == 0)
                            {
                                // I'll put a 2 or a 3
                                if (have2)
                                {
                                    // I'll play the 2, but I first have to find it so I can get rid of it
                                    int poz;
                                    bool found = false;

                                    for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                        if (EnemyHand[poz].symbol == "2") found = true;
                                    poz--;
                                    MessageBox.Show("The enemy countered the attack with a 2.");
                                    topcard = EnemyHand[poz];
                                    TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                    CardsAttack += 2;
                                    EliminateCardFromList(poz);
                                    EnemyMessage_Label.Hide();
                                    SwitchTurns();
                                    PlayGame();
                                }
                                else// if (have3)
                                {
                                    // I'll play the 3, but I first have to find it so I can get rid of it
                                    int poz;
                                    bool found = false;

                                    for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                        if (EnemyHand[poz].symbol == "3") found = true;
                                    poz--;
                                    MessageBox.Show("The enemy countered the attack with a 3.");
                                    topcard = EnemyHand[poz];
                                    TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                    CardsAttack += 3;
                                    EliminateCardFromList(poz);
                                    EnemyMessage_Label.Hide();
                                    SwitchTurns();
                                    PlayGame();
                                }
                            }
                            else
                            {
                                // I'll have to take the cards
                                MessageBox.Show("The enemy draw " + CardsAttack + " cards.");
                                while (CardsAttack > 0)
                                {
                                    DrawCard(Turn);
                                    CardsAttack--;
                                }
                                CardsLeft_Label.Text = "Cards Left: " + nrcards;
                                SwitchTurns();
                                PlayGame();
                            }
                        }
                        else if (have && have7)
                        {
                            // I'll have a 66% chance to respond
                            Random seed = new Random();
                            int choice = seed.Next(100);

                            if (choice % 3 == 1)
                            {
                                // I'll respind with 2 or 3 depending
                                if (have2)
                                {
                                    // I'll play the 2, but I first have to find it so I can get rid of it
                                    int poz;
                                    bool found = false;

                                    for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                        if (EnemyHand[poz].symbol == "2") found = true;
                                    poz--;
                                    MessageBox.Show("The enemy countered the attack with a 2.");
                                    topcard = EnemyHand[poz];
                                    TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                    CardsAttack += 2;
                                    EliminateCardFromList(poz);
                                    EnemyMessage_Label.Hide();
                                    SwitchTurns();
                                    PlayGame();
                                }
                                else// if (have3)
                                {
                                    // I'll play the 3, but I first have to find it so I can get rid of it
                                    int poz;
                                    bool found = false;

                                    for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                        if (EnemyHand[poz].symbol == "3") found = true;
                                    poz--;
                                    MessageBox.Show("The enemy countered the attack with a 3.");
                                    topcard = EnemyHand[poz];
                                    TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                    CardsAttack += 3;
                                    EliminateCardFromList(poz);
                                    EnemyMessage_Label.Hide();
                                    SwitchTurns();
                                    PlayGame();
                                }
                            }
                            else if (choice % 3 == 2)
                            {
                                // I'll play the 7, but I first have to find it so I can get rid of it
                                int poz;
                                bool found = false;

                                for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                    if (EnemyHand[poz].symbol == "7") found = true;
                                poz--;
                                MessageBox.Show("The enemy countered the attack with a 7.");
                                topcard = EnemyHand[poz];
                                TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                CardsAttack = 0;
                                EliminateCardFromList(poz);
                                EnemyMessage_Label.Hide();
                                SwitchTurns();
                                PlayGame();
                            }
                            else
                            {
                                // I'll have to take the cards
                                MessageBox.Show("The enemy draw " + CardsAttack + " cards.");
                                while (CardsAttack > 0)
                                {
                                    DrawCard(Turn);
                                    CardsAttack--;
                                }
                                CardsLeft_Label.Text = "Cards Left: " + nrcards;
                                SwitchTurns();
                                PlayGame();
                            }
                        }

                        //MessageBox.Show("Can Counter.");
                        //CardsAttack = 0;
                        //SwitchTurns();
                        //PlayGame();
                    }
                    else
                    {
                        //MessageBox.Show("Can't Counter.");
                        MessageBox.Show("The enemy drew " + CardsAttack + " cards.");
                        while (CardsAttack > 0)
                        {
                            DrawCard(Turn);
                            CardsAttack--;
                        }
                        CardsLeft_Label.Text = "Cards Left: " + nrcards;
                        SwitchTurns();
                        PlayGame();
                    }
                }
                else if (TurnsAttack > 0 && !Standing)
                {
                    if (!EnemyStanding)
                    {
                        bool have4 = false;
                        bool have7 = false;
                        int i;

                        for (i = 1; i <= EnemyHandNumber; i++)
                        {
                            if (EnemyHand[i].symbol == "4" && topcard.symbol == "4") have4 = true;
                            if (EnemyHand[i].symbol == "7") have7 = true;
                        }

                        if (!have4 && !have7)
                        {
                            EnemyStanding = true;
                            MessageBox.Show("The enemy decided to stand " + TurnsAttack + " turns.");
                            SwitchTurns();
                            PlayGame();
                        }
                        else if (have4 && !have7)
                        {
                            Random seed = new Random();
                            int choice = seed.Next(100);

                            if (choice % 2 == 0)
                            {
                                // I'll respond with 4
                                int poz;
                                bool found = false;

                                for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                    if (EnemyHand[poz].symbol == "4") found = true;

                                poz--;
                                topcard = EnemyHand[poz];
                                TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                TurnsAttack++;
                                EliminateCardFromList(poz);
                                EnemyMessage_Label.Hide();
                                SwitchTurns();
                                PlayGame();
                            }
                            else
                            {
                                EnemyStanding = true;
                                MessageBox.Show("The enemy decided to stand " + TurnsAttack + " turns.");
                                SwitchTurns();
                                PlayGame();
                            }
                        }
                        else if (!have4 && have7)
                        {
                            Random seed = new Random();
                            int choice = seed.Next(100);

                            if (choice % 2 == 0)
                            {
                                // I'll respond with 7
                                int poz;
                                bool found = false;

                                for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                    if (EnemyHand[poz].symbol == "7") found = true;

                                poz--;
                                topcard = EnemyHand[poz];
                                TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                TurnsAttack = 0;
                                EliminateCardFromList(poz);
                                EnemyMessage_Label.Hide();
                                SwitchTurns();
                                PlayGame();
                            }
                            else
                            {
                                EnemyStanding = true;
                                MessageBox.Show("The enemy decided to stand " + TurnsAttack + " turns.");
                                SwitchTurns();
                                PlayGame();
                            }
                        }
                        else
                        {
                            Random seed = new Random();
                            int choice = seed.Next(100);

                            if (choice % 5 == 1 || choice % 5 == 2)
                            {
                                // I'll respond with 7
                                int poz;
                                bool found = false;

                                for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                    if (EnemyHand[poz].symbol == "7") found = true;

                                poz--;
                                topcard = EnemyHand[poz];
                                TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                TurnsAttack = 0;
                                EliminateCardFromList(poz);
                                EnemyMessage_Label.Hide();
                                SwitchTurns();
                                PlayGame();
                            }
                            else if (choice % 5 == 3 || choice % 5 == 4)
                            {
                                // I'll respond with 4
                                int poz;
                                bool found = false;

                                for (poz = 1; poz <= EnemyHandNumber && !found; poz++)
                                    if (EnemyHand[poz].symbol == "4") found = true;

                                poz--;
                                topcard = EnemyHand[poz];
                                TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                                LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                                TurnsAttack++;
                                EliminateCardFromList(poz);
                                EnemyMessage_Label.Hide();
                                SwitchTurns();
                                PlayGame();
                            }
                            else
                            {
                                EnemyStanding = true;
                                MessageBox.Show("The enemy decided to stand " + TurnsAttack + " turns.");
                                SwitchTurns();
                                PlayGame();
                            }
                        }

                    }
                    else 
                    {
                        TurnsAttack--;
                        if (TurnsAttack > 0)
                        {
                            MessageBox.Show("The enemy has to stand " + TurnsAttack + " more turns.");
                            SwitchTurns();
                            PlayGame();
                        }
                        else
                        {
                            EnemyStanding = false;
                            EnemyPlay();
                        }
                    }
                }
                else if ((TurnsAttack == 0 && CardsAttack == 0) || (TurnsAttack > 0 && Standing))
                {
                    EnemyPlay();
                }
            }
        }

        private void EnemyPlay()
        {
            int i, j;
            int maximum = -1;
            int strikenr = 0;
            int[] pozt = new int[5];
            int[] actpoz = new int[5];

            for (i = 1; i <= EnemyHandNumber; i++)
                EnemyHand[i].CompatibilityCards();

            bool found = false;
            int signcode;

            for (i = 1; i <= EnemyHandNumber && !found; i++)
                if (EnemyHand[i].CompatibleWith(topcard)) found = true;

            if (found)
            {
                i--;

                if (EnemyHand[i].symbol == "2")
                {
                    topcard = EnemyHand[i];
                    TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                    CardsAttack += 2;
                    EliminateCardFromList(i);
                    SwitchTurns();
                    PlayGame();
                }
                else if (EnemyHand[i].symbol == "3")
                {
                    topcard = EnemyHand[i];
                    TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                    CardsAttack += 3;
                    EliminateCardFromList(i);
                    SwitchTurns();
                    PlayGame();
                }
                else if (EnemyHand[i].symbol == "4")
                {
                    topcard = EnemyHand[i];
                    TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                    TurnsAttack++;
                    EliminateCardFromList(i);
                    SwitchTurns();
                    PlayGame();
                }
                else if (EnemyHand[i].symbol == "7")
                {
                    if (TurnsAttack > 0 && topcard.symbol == "4")
                    {
                        MessageBox.Show("The enemy countered the turns attack with a 7.");
                        TurnsAttack = 0;
                        topcard = EnemyHand[i];
                        TopCard.Image = Image.FromFile(topcard.GetNameOfCard(topcard));
                        LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                        EliminateCardFromList(i);
                        SwitchTurns();
                        PlayGame();
                    }
                    else if (CardsAttack > 0 && (topcard.symbol == "2" || topcard.symbol == "3"))
                    {
                        MessageBox.Show("The enemy countered the cards attack with a 7.");
                        CardsAttack = 0;
                        topcard = EnemyHand[i];
                        TopCard.Image = Image.FromFile(topcard.GetNameOfCard(topcard));
                        LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                        EliminateCardFromList(i);
                        SwitchTurns();
                        PlayGame();
                    }
                    else
                    {
                        topcard = EnemyHand[i];
                        TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                        LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                        EliminateCardFromList(i);
                        SwitchTurns();
                        PlayGame();
                    }
                }
                else if (EnemyHand[i].symbol == "A")
                {
                    // I need to know in which symbol he changes
                    int nrmax = -1;
                    int nr;
                    int k;
                    string signtext;

                    //checking the hearts sign
                    nr = 0;
                    for (k = 1; k <= EnemyHandNumber; k++)
                        if (k == i) continue;
                        else { if (EnemyHand[i].signcode == 3) nr++; }
                    if (nr > nrmax) { nrmax = nr; signcode = 3; topcard.signcode = signcode; signtext = "hearts"; AceSignCode.Text = "Changed symbol into:\n" + signtext; }

                    //checking the diamonds sign
                    nr = 0;
                    for (k = 1; k <= EnemyHandNumber; k++)
                        if (k == i) continue;
                        else { if (EnemyHand[i].signcode == 4) nr++; }
                    if (nr > nrmax) { nrmax = nr; signcode = 4; topcard.signcode = signcode; signtext = "diamonds"; AceSignCode.Text = "Changed symbol into:\n" + signtext; }

                    //checking the clubs sign
                    nr = 0;
                    for (k = 1; k <= EnemyHandNumber; k++)
                        if (k == i) continue;
                        else { if (EnemyHand[i].signcode == 5) nr++; }
                    if (nr > nrmax) { nrmax = nr; signcode = 5; topcard.signcode = signcode; signtext = "clubs"; AceSignCode.Text = "Changed symbol into:\n" + signtext; }

                    //checking the spades sign
                    nr = 0;
                    for (k = 1; k <= EnemyHandNumber; k++)
                        if (k == i) continue;
                        else { if (EnemyHand[i].signcode == 6) nr++; }
                    if (nr > nrmax) { nrmax = nr; signcode = 6; topcard.signcode = signcode; signtext = "spades"; AceSignCode.Text = "Changed symbol into:\n" + signtext; }

                    topcard.symbol = EnemyHand[i].symbol;
                    TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + EnemyHand[i].GetNameOfCard(EnemyHand[i]);
                    EliminateCardFromList(i);
                    SwitchTurns();
                    PlayGame();
                }
                else
                {
                    topcard = EnemyHand[i];
                    TopCard.Image = Image.FromFile(topcard.GetNameOfFile());
                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                    EliminateCardFromList(i);
                    SwitchTurns();
                    PlayGame();
                }
            }
            else
            {
                DrawCard(Turn);
                CardsLeft_Label.Text = "Cards Left: " + nrcards;
                MessageBox.Show("The enemy drew one card.");
                SwitchTurns();
                PlayGame();
            }
        }

        private void EliminateCardFromList(int poz)
        {
            Card c = new Card();
            int i;

            for (i = poz; i < EnemyHandNumber; i++)
                EnemyHand[i] = EnemyHand[i + 1];
            EnemyHand[EnemyHandNumber] = c;
            EnemyHandNumber--;
        }

        private void DrawCard(bool turn)
        {
            if (turn)
            {
                if (nrcards > 0)
                {
                    MyHandNumber++;
                    MyHand[MyHandNumber] = deck.cards[nrcards];
                    nrcards--;
                    MyCards_ComboBox.Items.Add(MyHand[MyHandNumber].GetNameOfCard(MyHand[MyHandNumber]));
                }
                else
                {
                    nrcards = 41;
                    deck.RegenerateCards(MyHand,MyHandNumber,EnemyHand,EnemyHandNumber);
                    CardsLeft_Label.Text = "Cards Left: " + nrcards;
                }
            }
            else
            {
                if (nrcards > 0)
                {
                    EnemyHandNumber++;
                    EnemyHand[EnemyHandNumber] = deck.cards[nrcards];
                    nrcards--;
                }
                else
                {
                    nrcards = 41;
                    deck.RegenerateCards(MyHand, MyHandNumber, EnemyHand, EnemyHandNumber);
                    CardsLeft_Label.Text = "Cards Left: " + nrcards;
                }
            }
        }

        private void Quit_Button_Click(object sender, EventArgs e)
        {
            MainMenuForm f = new MainMenuForm();

            f.Show();
            this.Close();
        }

        private void EndTurn_Button_Click(object sender, EventArgs e)
        {
            if (nrcards > 0)
            {
                MyHandNumber++;
                MyHand[MyHandNumber] = deck.cards[nrcards];
                nrcards--;
                MyCards_ComboBox.Items.Add(MyHand[MyHandNumber].GetNameOfCard(MyHand[MyHandNumber]));
                CardsLeft_Label.Text = "Cards Left: " + nrcards;
                SwitchTurns();
                PlayGame();
            }
            else
            {
                nrcards = 41;
                deck.RegenerateCards(MyHand, MyHandNumber, EnemyHand, EnemyHandNumber);
                CardsLeft_Label.Text = "Cards Left: " + nrcards;
            }
        }

        private void InsertQueue_Button_Click(object sender, EventArgs e)
        {
            string CardName;
            
            if (!(MyCards_ComboBox.SelectedIndex > -1)) MessageBox.Show("Select a card from the list to the left.");
            else
            {
                CardName = MyCards_ComboBox.Text;
                nrCardsQueue++;
                QueueCard[nrCardsQueue] = GetCard(CardName);
                ChangeQueueCardsText();
                MyCards_ComboBox.Items.Remove(CardName);
            }
        }

        private void DeleteLastCard_Button_Click(object sender, EventArgs e)
        {
            string CardName;

            if (nrCardsQueue == 0) MessageBox.Show("You have no cards in queue.");
            else
            {
                CardName = QueueCard[nrCardsQueue].GetNameOfCurrentCard();
                nrCardsQueue--;
                ChangeQueueCardsText();
                MyCards_ComboBox.Items.Add(CardName);
            }
        }

        private void ChangeQueueCardsText()
        {
            int i;

            QueueCards_Label.Text.Replace(QueueCards_Label.Text, "");
            QueueCards_Label.Text = "Play these cards:" + "\n";
            for (i = 1; i <= nrCardsQueue; i++)
                QueueCards_Label.Text += (QueueCard[i].GetNameOfCurrentCard() + "\n");
        }

        private Card GetCard(string name)
        {
            Card c = new Card();

            if (name[0] == '2')
            {
                c.symbol = "2";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == '3')
            {
                c.symbol = "3";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == '4')
            {
                c.symbol = "4";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == '5')
            {
                c.symbol = "5";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == '6')
            {
                c.symbol = "6";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == '7')
            {
                c.symbol = "7";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == '8')
            {
                c.symbol = "8";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == '9')
            {
                c.symbol = "9";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == 'J')
            {
                c.symbol = "J";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == 'Q')
            {
                c.symbol = "Q";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == 'K')
            {
                c.symbol = "K";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == 'A')
            {
                c.symbol = "A";
                if (name[5] == 'h') c.signcode = 3;
                else if (name[5] == 'd') c.signcode = 4;
                else if (name[5] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }
            else if (name[0] == '1')
            {
                c.symbol = "10";
                if (name[6] == 'h') c.signcode = 3;
                else if (name[6] == 'd') c.signcode = 4;
                else if (name[6] == 'c') c.signcode = 5;
                else c.signcode = 6;
            }

            return c;
        }

        private void DrawAttackCards_Button_Click(object sender, EventArgs e)
        {
            for (int j = 1; j <= CardsAttack; j++)
                DrawCard(Turn);
            CardsAttack = 0;
            DrawAttackCards_Button.Hide();
            CardsLeft_Label.Text = "Cards Left: " + nrcards;
            SwitchTurns();
            PlayGame();
        }

        private void StandTurn_Button_Click(object sender, EventArgs e)
        {
            Standing = true;
            SwitchTurns();
            StandTurn_Button.Hide();
            PlayGame();
        }

        private void InitQueue()
        {
            QueueCard[1] = new Card();
            QueueCard[2] = new Card();
            QueueCard[3] = new Card();
            QueueCard[4] = new Card();
        }

        private void PlayCards_Button_Click(object sender, EventArgs e)
        {
            int i;

            for (i = 1; i <= nrCardsQueue; i++)
                QueueCard[i].CompatibilityCards();

            if (nrCardsQueue == 1)
            {
                // I have to check if the played card is compatible with the top card
                bool compat = QueueCard[1].CompatibleWith(topcard);

                if (QueueCard[1].symbol == "2" && TurnsAttack == 0)
                {
                    if (ace)
                    {
                        ace = false;
                        AceSignCode.Hide();
                    }
                    topcard = QueueCard[1];
                    TopCard.Image = Image.FromFile(QueueCard[1].GetNameOfFile());
                    CardsAttack += 2;
                    InitQueue();
                    nrCardsQueue = 0;
                    MyHandNumber--;
                    QueueCards_Label.Text = "Play these cards:\n";
                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                    SwitchTurns();
                    PlayGame();
                }
                else if (QueueCard[1].symbol == "3" && TurnsAttack == 0)
                {
                    if (ace)
                    {
                        ace = false;
                        AceSignCode.Hide();
                    }
                    topcard = QueueCard[1];
                    TopCard.Image = Image.FromFile(QueueCard[1].GetNameOfFile());
                    CardsAttack += 3;
                    InitQueue();
                    nrCardsQueue = 0;
                    MyHandNumber--;
                    QueueCards_Label.Text = "Play these cards:\n";
                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                    SwitchTurns();
                    PlayGame();
                }
                else if (QueueCard[1].symbol == "7")
                {
                    if (ace)
                    {
                        ace = false;
                        AceSignCode.Hide();
                    }
                    if (TurnsAttack > 0 && topcard.symbol == "4") TurnsAttack = 0;
                    if (CardsAttack > 0 && (topcard.symbol == "2" || topcard.symbol == "3")) CardsAttack = 0;
                    topcard = QueueCard[1];
                    TopCard.Image = Image.FromFile(QueueCard[1].GetNameOfFile());
                    InitQueue();
                    nrCardsQueue = 0;
                    MyHandNumber--;
                    QueueCards_Label.Text = "Play these cards:\n";
                    LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                    SwitchTurns();
                    PlayGame();
                }
                else if (compat)
                {
                    if (QueueCard[1].symbol == "4")
                    {
                        if (CardsAttack > 0)
                        {
                            MessageBox.Show("You've been attacked!\nYou cannot play this card!");
                        }
                        else
                        {
                            TopCard.Image = Image.FromFile(QueueCard[1].GetNameOfFile());
                            topcard = QueueCard[1];
                            if (ace)
                            {
                                ace = false;
                                AceSignCode.Hide();
                            }
                            TurnsAttack++;
                            InitQueue();
                            nrCardsQueue = 0;
                            MyHandNumber--;
                            QueueCards_Label.Text = "Play these cards:\n";
                            LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                            SwitchTurns();
                            PlayGame();
                        }
                    }
                    else if (QueueCard[1].symbol == "A")
                    {
                        if (CardsAttack > 0 || (TurnsAttack > 0 && (!EnemyStanding && !Standing)))
                        {
                            MessageBox.Show("You've been attacked!\nYou cannot play this card!");
                        }
                        else
                        {
                            TopCard.Image = Image.FromFile(QueueCard[1].GetNameOfFile());
                            topcard = QueueCard[1];
                            ace = true;
                            MyHandNumber--;
                            QueueCard[1] = new Card();
                            QueueCard[2] = new Card();
                            QueueCard[3] = new Card();
                            QueueCard[4] = new Card();
                            nrCardsQueue = 0;
                            QueueCards_Label.Text = "Play these cards:\n";
                            LastCardPlayed_Label.Text = "Last Cards Played:\n" + topcard.GetNameOfCard(topcard);
                            ChangeSignCode_Panel.Show();
                            AceSignCode.Show();
                            PlayGame();
                        }
                    }
                    else
                    {
                        if (CardsAttack > 0 || (TurnsAttack > 0 && (!EnemyStanding && !Standing)))
                        {
                            MessageBox.Show("You've been attacked!\nYou cannot play this card!");
                        }
                        else
                        {
                            TopCard.Image = Image.FromFile(QueueCard[1].GetNameOfFile());
                            topcard = QueueCard[1];
                            if (ace)
                            {
                                ace = false;
                                AceSignCode.Hide();
                            }
                            LastCardPlayed_Label.Text = "Last Cards Played:\n" + QueueCard[1].GetNameOfCard(QueueCard[1]);
                            QueueCards_Label.Text = "Play these cards:\n";
                            QueueCard[1] = new Card();
                            QueueCard[2] = new Card();
                            QueueCard[3] = new Card();
                            QueueCard[4] = new Card();
                            nrCardsQueue = 0;
                            MyHandNumber--;
                            SwitchTurns();
                            PlayGame();
                        }
                    }
                }
                else MessageBox.Show("You cannot play this card!");
            }
            else if (nrCardsQueue > 1)
            {
                // I have to check if all the cards are compatible
                // That means: the first card in the queue to be compatible with the top card
                // And the rest of the cards to have the same symbol

                bool compat = QueueCard[1].CompatibleWith(topcard);

                if (compat)
                {
                    // I have to check if the rest of the carsd have the same symbol as the first one
                    string defaultSymbol = QueueCard[1].symbol;
                    string lastcards = "\n";
                    bool pass = true;

                    for (i = 1; i <= nrCardsQueue; i++)
                        if (QueueCard[i].symbol != defaultSymbol) pass = false;
                        else lastcards += (QueueCard[i].GetNameOfCard(QueueCard[i]) + "\n");

                    if (!pass) MessageBox.Show("All the cards need to have the same symbol.");
                    else
                    {
                        if (defaultSymbol == "2")
                        {
                            for (i = 1; i <= nrCardsQueue; i++)
                                CardsAttack += 2;
                            if (ace)
                            {
                                ace = false;
                                AceSignCode.Hide();
                            }
                        }
                        else if (defaultSymbol == "3")
                        {
                            for (i = 1; i <= nrCardsQueue; i++)
                                CardsAttack += 3;
                            if (ace)
                            {
                                ace = false;
                                AceSignCode.Hide();
                            }
                        }
                        else if (defaultSymbol == "4")
                        {
                            for (i = 1; i <= nrCardsQueue; i++)
                                TurnsAttack++;
                            if (ace)
                            {
                                ace = false;
                                AceSignCode.Hide();
                            }
                        }
                        else if (defaultSymbol == "7")
                        {
                            if (topcard.symbol == "2" || topcard.symbol == "3") CardsAttack = 0;
                            else if (topcard.symbol == "4") TurnsAttack = 0;
                            if (ace)
                            {
                                ace = false;
                                AceSignCode.Hide();
                            }
                        }

                        if (ace)
                        {
                            ace = false;
                            AceSignCode.Hide();
                        }
                        TopCard.Image = Image.FromFile(QueueCard[nrCardsQueue].GetNameOfFile());
                        topcard = QueueCard[nrCardsQueue];
                        LastCardPlayed_Label.Text = "Last Cards Played:" + lastcards;
                        QueueCards_Label.Text = "Play these cards:\n";
                        for (i = 1; i <= nrCardsQueue; i++)
                            QueueCards_Label.Text += (QueueCard[i].GetNameOfCard(QueueCard[1]) + "\n");

                        InitQueue();
                        MyHandNumber -= nrCardsQueue;
                        nrCardsQueue = 0;
                        SwitchTurns();
                        PlayGame();
                    }
                }
                else MessageBox.Show("You can't play these cards. Remake the queue.");
            }
        }

        private void Spades_Click(object sender, EventArgs e)
        {
            topcard.signcode = 6;
            AceSignCode.Text = "Changed sign into: \n Spades";
            ChangeSignCode_Panel.Hide();
            SwitchTurns();
            PlayGame();
        }

        private void Clubs_Click(object sender, EventArgs e)
        {
            topcard.signcode = 5;
            AceSignCode.Text = "Changed sign into: \n Clubs";
            ChangeSignCode_Panel.Hide();
            SwitchTurns();
            PlayGame();
        }

        private void Diamonds_Click(object sender, EventArgs e)
        {
            topcard.signcode = 4;
            AceSignCode.Text = "Changed sign into: \n Diamonds";
            ChangeSignCode_Panel.Hide();
            SwitchTurns();
            PlayGame();
        }

        private void Hearts_Click(object sender, EventArgs e)
        {
            topcard.signcode = 3;
            AceSignCode.Text = "Changed sign into: \n Hearts";
            ChangeSignCode_Panel.Hide();
            SwitchTurns();
            PlayGame();
        }

        private void SwitchTurns()
        {
            if (!Turn)
            {
                Turn = true;
                nrTurns++;
            }
            else
            {
                /*
                sec = 0;
                if (!loadEnemyWait)
                {
                    EnemyWait.Interval = 1000;
                    EnemyWait.Tick += new EventHandler(EnemyWait_Tick);
                    loadEnemyWait = true;
                }
                EnemyWait.Start();
                */
                Turn = false;
            }
        }

        private void EnemyWait_Tick(object sender, EventArgs e)
        {
            if (sec == 3)
            {
                MessageBox.Show("Timer stopped.");
                EnemyWait.Stop();
            }
            else sec++;
        }
    }
}
