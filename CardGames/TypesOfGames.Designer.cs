﻿namespace CardGames
{
    partial class TypesOfGames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TypesOfGames));
            this.label1 = new System.Windows.Forms.Label();
            this.MacaoLabel = new System.Windows.Forms.Label();
            this.BlackJackLabel = new System.Windows.Forms.Label();
            this.MacaoHTP_Label = new System.Windows.Forms.Label();
            this.BlackJackHTP_Label = new System.Windows.Forms.Label();
            this.BackToMainMenu_Button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.ReadThis_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Sitka Display", 30F);
            this.label1.Location = new System.Drawing.Point(50, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(451, 58);
            this.label1.TabIndex = 0;
            this.label1.Text = "What do you want to play?";
            // 
            // MacaoLabel
            // 
            this.MacaoLabel.AutoSize = true;
            this.MacaoLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MacaoLabel.Font = new System.Drawing.Font("Sitka Display", 20F);
            this.MacaoLabel.Location = new System.Drawing.Point(55, 100);
            this.MacaoLabel.Name = "MacaoLabel";
            this.MacaoLabel.Size = new System.Drawing.Size(86, 39);
            this.MacaoLabel.TabIndex = 1;
            this.MacaoLabel.Text = "Macao";
            this.MacaoLabel.Click += new System.EventHandler(this.MacaoLabel_Click);
            this.MacaoLabel.MouseEnter += new System.EventHandler(this.MacaoLabel_MouseEnter);
            this.MacaoLabel.MouseLeave += new System.EventHandler(this.MacaoLabel_MouseLeave);
            // 
            // BlackJackLabel
            // 
            this.BlackJackLabel.AutoSize = true;
            this.BlackJackLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BlackJackLabel.Font = new System.Drawing.Font("Sitka Display", 20F);
            this.BlackJackLabel.Location = new System.Drawing.Point(55, 176);
            this.BlackJackLabel.Name = "BlackJackLabel";
            this.BlackJackLabel.Size = new System.Drawing.Size(124, 39);
            this.BlackJackLabel.TabIndex = 2;
            this.BlackJackLabel.Text = "Black Jack";
            this.BlackJackLabel.Click += new System.EventHandler(this.BlackJackLabel_Click);
            // 
            // MacaoHTP_Label
            // 
            this.MacaoHTP_Label.AutoSize = true;
            this.MacaoHTP_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MacaoHTP_Label.Font = new System.Drawing.Font("Sitka Display", 15F);
            this.MacaoHTP_Label.Location = new System.Drawing.Point(274, 110);
            this.MacaoHTP_Label.Name = "MacaoHTP_Label";
            this.MacaoHTP_Label.Size = new System.Drawing.Size(118, 29);
            this.MacaoHTP_Label.TabIndex = 3;
            this.MacaoHTP_Label.Text = "How to play?";
            this.MacaoHTP_Label.Click += new System.EventHandler(this.MacaoHTP_Label_Click);
            // 
            // BlackJackHTP_Label
            // 
            this.BlackJackHTP_Label.AutoSize = true;
            this.BlackJackHTP_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BlackJackHTP_Label.Font = new System.Drawing.Font("Sitka Display", 15F);
            this.BlackJackHTP_Label.Location = new System.Drawing.Point(274, 184);
            this.BlackJackHTP_Label.Name = "BlackJackHTP_Label";
            this.BlackJackHTP_Label.Size = new System.Drawing.Size(118, 29);
            this.BlackJackHTP_Label.TabIndex = 4;
            this.BlackJackHTP_Label.Text = "How to play?";
            this.BlackJackHTP_Label.Click += new System.EventHandler(this.BlackJackHTP_Label_Click);
            // 
            // BackToMainMenu_Button
            // 
            this.BackToMainMenu_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BackToMainMenu_Button.Font = new System.Drawing.Font("Sitka Display", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackToMainMenu_Button.Location = new System.Drawing.Point(458, 240);
            this.BackToMainMenu_Button.Name = "BackToMainMenu_Button";
            this.BackToMainMenu_Button.Size = new System.Drawing.Size(88, 48);
            this.BackToMainMenu_Button.TabIndex = 5;
            this.BackToMainMenu_Button.Text = "Back to Main Menu";
            this.BackToMainMenu_Button.UseVisualStyleBackColor = true;
            this.BackToMainMenu_Button.Click += new System.EventHandler(this.BackToMainMenu_Button_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label3.Font = new System.Drawing.Font("Sitka Display", 15F);
            this.label3.Location = new System.Drawing.Point(61, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(220, 29);
            this.label3.TabIndex = 6;
            this.label3.Text = "More games coming soon";
            // 
            // ReadThis_Label
            // 
            this.ReadThis_Label.AutoSize = true;
            this.ReadThis_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReadThis_Label.Font = new System.Drawing.Font("Sitka Display", 10F);
            this.ReadThis_Label.Location = new System.Drawing.Point(398, 192);
            this.ReadThis_Label.Name = "ReadThis_Label";
            this.ReadThis_Label.Size = new System.Drawing.Size(149, 20);
            this.ReadThis_Label.TabIndex = 7;
            this.ReadThis_Label.Text = "Read this before you play!";
            this.ReadThis_Label.Click += new System.EventHandler(this.ReadThis_Label_Click);
            // 
            // TypesOfGames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(558, 300);
            this.Controls.Add(this.ReadThis_Label);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BackToMainMenu_Button);
            this.Controls.Add(this.BlackJackHTP_Label);
            this.Controls.Add(this.MacaoHTP_Label);
            this.Controls.Add(this.BlackJackLabel);
            this.Controls.Add(this.MacaoLabel);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TypesOfGames";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Choose a Game";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label MacaoLabel;
        private System.Windows.Forms.Label BlackJackLabel;
        private System.Windows.Forms.Label MacaoHTP_Label;
        private System.Windows.Forms.Label BlackJackHTP_Label;
        private System.Windows.Forms.Button BackToMainMenu_Button;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label ReadThis_Label;
    }
}