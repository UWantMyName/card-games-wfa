﻿namespace CardGames
{
    partial class BlackJack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlackJack));
            this.BetSum_Label = new System.Windows.Forms.Label();
            this.PlayGame_Button = new System.Windows.Forms.Button();
            this.ECard1_Picture = new System.Windows.Forms.PictureBox();
            this.MCard1_Picture = new System.Windows.Forms.PictureBox();
            this.ECard2_Picture = new System.Windows.Forms.PictureBox();
            this.MCard2_Picture = new System.Windows.Forms.PictureBox();
            this.SoundPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.Stand_Button = new System.Windows.Forms.Button();
            this.Hit_Button = new System.Windows.Forms.Button();
            this.Double_Button = new System.Windows.Forms.Button();
            this.Split_Button = new System.Windows.Forms.Button();
            this.Quit_Button = new System.Windows.Forms.Button();
            this.BetPlacing_Text = new System.Windows.Forms.Label();
            this.ClearBet_Button = new System.Windows.Forms.Button();
            this.BankSum_Label = new System.Windows.Forms.Label();
            this.BetSum_TextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.ECard1_Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MCard1_Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ECard2_Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MCard2_Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoundPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // BetSum_Label
            // 
            this.BetSum_Label.AutoSize = true;
            this.BetSum_Label.BackColor = System.Drawing.Color.Transparent;
            this.BetSum_Label.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.BetSum_Label.ForeColor = System.Drawing.Color.White;
            this.BetSum_Label.Location = new System.Drawing.Point(476, 48);
            this.BetSum_Label.Name = "BetSum_Label";
            this.BetSum_Label.Size = new System.Drawing.Size(110, 22);
            this.BetSum_Label.TabIndex = 10;
            this.BetSum_Label.Text = "Bet Sum: 0$";
            this.BetSum_Label.Visible = false;
            // 
            // PlayGame_Button
            // 
            this.PlayGame_Button.BackColor = System.Drawing.Color.Transparent;
            this.PlayGame_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PlayGame_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.PlayGame_Button.Location = new System.Drawing.Point(198, 224);
            this.PlayGame_Button.Name = "PlayGame_Button";
            this.PlayGame_Button.Size = new System.Drawing.Size(75, 23);
            this.PlayGame_Button.TabIndex = 11;
            this.PlayGame_Button.Text = "Let\'s Play!";
            this.PlayGame_Button.UseVisualStyleBackColor = false;
            this.PlayGame_Button.Visible = false;
            this.PlayGame_Button.Click += new System.EventHandler(this.PlayGame_Button_Click);
            // 
            // ECard1_Picture
            // 
            this.ECard1_Picture.BackColor = System.Drawing.Color.Transparent;
            this.ECard1_Picture.Image = ((System.Drawing.Image)(resources.GetObject("ECard1_Picture.Image")));
            this.ECard1_Picture.Location = new System.Drawing.Point(26, 27);
            this.ECard1_Picture.Name = "ECard1_Picture";
            this.ECard1_Picture.Size = new System.Drawing.Size(125, 180);
            this.ECard1_Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ECard1_Picture.TabIndex = 12;
            this.ECard1_Picture.TabStop = false;
            this.ECard1_Picture.Visible = false;
            // 
            // MCard1_Picture
            // 
            this.MCard1_Picture.BackColor = System.Drawing.Color.Transparent;
            this.MCard1_Picture.Location = new System.Drawing.Point(26, 264);
            this.MCard1_Picture.Name = "MCard1_Picture";
            this.MCard1_Picture.Size = new System.Drawing.Size(125, 180);
            this.MCard1_Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MCard1_Picture.TabIndex = 13;
            this.MCard1_Picture.TabStop = false;
            this.MCard1_Picture.Visible = false;
            // 
            // ECard2_Picture
            // 
            this.ECard2_Picture.BackColor = System.Drawing.Color.Transparent;
            this.ECard2_Picture.Image = ((System.Drawing.Image)(resources.GetObject("ECard2_Picture.Image")));
            this.ECard2_Picture.Location = new System.Drawing.Point(60, 24);
            this.ECard2_Picture.Name = "ECard2_Picture";
            this.ECard2_Picture.Size = new System.Drawing.Size(125, 180);
            this.ECard2_Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ECard2_Picture.TabIndex = 14;
            this.ECard2_Picture.TabStop = false;
            this.ECard2_Picture.Visible = false;
            // 
            // MCard2_Picture
            // 
            this.MCard2_Picture.BackColor = System.Drawing.Color.Transparent;
            this.MCard2_Picture.Location = new System.Drawing.Point(60, 261);
            this.MCard2_Picture.Name = "MCard2_Picture";
            this.MCard2_Picture.Size = new System.Drawing.Size(125, 180);
            this.MCard2_Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MCard2_Picture.TabIndex = 15;
            this.MCard2_Picture.TabStop = false;
            this.MCard2_Picture.Visible = false;
            // 
            // SoundPlayer
            // 
            this.SoundPlayer.Enabled = true;
            this.SoundPlayer.Location = new System.Drawing.Point(638, 132);
            this.SoundPlayer.Name = "SoundPlayer";
            this.SoundPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("SoundPlayer.OcxState")));
            this.SoundPlayer.Size = new System.Drawing.Size(75, 23);
            this.SoundPlayer.TabIndex = 16;
            this.SoundPlayer.Visible = false;
            // 
            // Stand_Button
            // 
            this.Stand_Button.BackColor = System.Drawing.Color.Transparent;
            this.Stand_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Stand_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.Stand_Button.Location = new System.Drawing.Point(527, 273);
            this.Stand_Button.Name = "Stand_Button";
            this.Stand_Button.Size = new System.Drawing.Size(75, 23);
            this.Stand_Button.TabIndex = 17;
            this.Stand_Button.Text = "Stand";
            this.Stand_Button.UseVisualStyleBackColor = false;
            this.Stand_Button.Visible = false;
            this.Stand_Button.Click += new System.EventHandler(this.Stand_Button_Click);
            // 
            // Hit_Button
            // 
            this.Hit_Button.BackColor = System.Drawing.Color.Transparent;
            this.Hit_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Hit_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.Hit_Button.Location = new System.Drawing.Point(443, 273);
            this.Hit_Button.Name = "Hit_Button";
            this.Hit_Button.Size = new System.Drawing.Size(75, 23);
            this.Hit_Button.TabIndex = 18;
            this.Hit_Button.Text = "Hit";
            this.Hit_Button.UseVisualStyleBackColor = false;
            this.Hit_Button.Visible = false;
            this.Hit_Button.Click += new System.EventHandler(this.Hit_Button_Click);
            // 
            // Double_Button
            // 
            this.Double_Button.BackColor = System.Drawing.Color.Transparent;
            this.Double_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Double_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.Double_Button.Location = new System.Drawing.Point(443, 315);
            this.Double_Button.Name = "Double_Button";
            this.Double_Button.Size = new System.Drawing.Size(75, 23);
            this.Double_Button.TabIndex = 19;
            this.Double_Button.Text = "Double";
            this.Double_Button.UseVisualStyleBackColor = false;
            this.Double_Button.Visible = false;
            this.Double_Button.Click += new System.EventHandler(this.Double_Button_Click);
            // 
            // Split_Button
            // 
            this.Split_Button.BackColor = System.Drawing.Color.Transparent;
            this.Split_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Split_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.Split_Button.Location = new System.Drawing.Point(527, 315);
            this.Split_Button.Name = "Split_Button";
            this.Split_Button.Size = new System.Drawing.Size(75, 23);
            this.Split_Button.TabIndex = 20;
            this.Split_Button.Text = "Split";
            this.Split_Button.UseVisualStyleBackColor = false;
            this.Split_Button.Visible = false;
            this.Split_Button.Click += new System.EventHandler(this.Split_Button_Click);
            // 
            // Quit_Button
            // 
            this.Quit_Button.BackColor = System.Drawing.Color.Transparent;
            this.Quit_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Quit_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.Quit_Button.Location = new System.Drawing.Point(526, 12);
            this.Quit_Button.Name = "Quit_Button";
            this.Quit_Button.Size = new System.Drawing.Size(75, 23);
            this.Quit_Button.TabIndex = 21;
            this.Quit_Button.Text = "Quit";
            this.Quit_Button.UseVisualStyleBackColor = false;
            this.Quit_Button.Click += new System.EventHandler(this.Quit_Button_Click);
            // 
            // BetPlacing_Text
            // 
            this.BetPlacing_Text.AutoSize = true;
            this.BetPlacing_Text.BackColor = System.Drawing.Color.Transparent;
            this.BetPlacing_Text.Font = new System.Drawing.Font("Modern No. 20", 20F);
            this.BetPlacing_Text.ForeColor = System.Drawing.Color.White;
            this.BetPlacing_Text.Location = new System.Drawing.Point(125, 139);
            this.BetPlacing_Text.Name = "BetPlacing_Text";
            this.BetPlacing_Text.Size = new System.Drawing.Size(375, 29);
            this.BetPlacing_Text.TabIndex = 22;
            this.BetPlacing_Text.Text = "Make a bet between 2$ and 500$";
            // 
            // ClearBet_Button
            // 
            this.ClearBet_Button.BackColor = System.Drawing.Color.Transparent;
            this.ClearBet_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ClearBet_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.ClearBet_Button.Location = new System.Drawing.Point(526, 375);
            this.ClearBet_Button.Name = "ClearBet_Button";
            this.ClearBet_Button.Size = new System.Drawing.Size(75, 23);
            this.ClearBet_Button.TabIndex = 23;
            this.ClearBet_Button.Text = "Clear";
            this.ClearBet_Button.UseVisualStyleBackColor = false;
            this.ClearBet_Button.Visible = false;
            this.ClearBet_Button.Click += new System.EventHandler(this.ClearBet_Button_Click);
            // 
            // BankSum_Label
            // 
            this.BankSum_Label.AutoSize = true;
            this.BankSum_Label.BackColor = System.Drawing.Color.Transparent;
            this.BankSum_Label.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.BankSum_Label.ForeColor = System.Drawing.Color.White;
            this.BankSum_Label.Location = new System.Drawing.Point(476, 85);
            this.BankSum_Label.Name = "BankSum_Label";
            this.BankSum_Label.Size = new System.Drawing.Size(110, 22);
            this.BankSum_Label.TabIndex = 24;
            this.BankSum_Label.Text = "In Bank: 0$";
            this.BankSum_Label.Visible = false;
            // 
            // BetSum_TextBox
            // 
            this.BetSum_TextBox.Location = new System.Drawing.Point(294, 226);
            this.BetSum_TextBox.Multiline = true;
            this.BetSum_TextBox.Name = "BetSum_TextBox";
            this.BetSum_TextBox.Size = new System.Drawing.Size(100, 20);
            this.BetSum_TextBox.TabIndex = 25;
            // 
            // BlackJack
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(614, 460);
            this.Controls.Add(this.BetSum_TextBox);
            this.Controls.Add(this.BankSum_Label);
            this.Controls.Add(this.ClearBet_Button);
            this.Controls.Add(this.Quit_Button);
            this.Controls.Add(this.Split_Button);
            this.Controls.Add(this.Double_Button);
            this.Controls.Add(this.Hit_Button);
            this.Controls.Add(this.Stand_Button);
            this.Controls.Add(this.SoundPlayer);
            this.Controls.Add(this.MCard2_Picture);
            this.Controls.Add(this.ECard2_Picture);
            this.Controls.Add(this.PlayGame_Button);
            this.Controls.Add(this.BetSum_Label);
            this.Controls.Add(this.MCard1_Picture);
            this.Controls.Add(this.ECard1_Picture);
            this.Controls.Add(this.BetPlacing_Text);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlackJack";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Black Jack";
            ((System.ComponentModel.ISupportInitialize)(this.ECard1_Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MCard1_Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ECard2_Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MCard2_Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoundPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label BetSum_Label;
        private System.Windows.Forms.Button PlayGame_Button;
        private System.Windows.Forms.PictureBox ECard1_Picture;
        private System.Windows.Forms.PictureBox MCard1_Picture;
        private System.Windows.Forms.PictureBox ECard2_Picture;
        private System.Windows.Forms.PictureBox MCard2_Picture;
        private AxWMPLib.AxWindowsMediaPlayer SoundPlayer;
        private System.Windows.Forms.Button Stand_Button;
        private System.Windows.Forms.Button Hit_Button;
        private System.Windows.Forms.Button Double_Button;
        private System.Windows.Forms.Button Split_Button;
        private System.Windows.Forms.Button Quit_Button;
        private System.Windows.Forms.Label BetPlacing_Text;
        private System.Windows.Forms.Button ClearBet_Button;
        private System.Windows.Forms.Label BankSum_Label;
        private System.Windows.Forms.TextBox BetSum_TextBox;
    }
}