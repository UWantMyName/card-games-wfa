﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardGames
{
    public partial class TypesOfGames : Form
    {
        public TypesOfGames()
        {
            InitializeComponent();
        }

        private void MacaoLabel_Click(object sender, EventArgs e)
        {
            Macao f = new Macao();

            f.Show();
            this.Hide();
        }

        private void MacaoLabel_MouseEnter(object sender, EventArgs e)
        {
            MacaoLabel.Font = new Font("Sitka Display", 20, FontStyle.Bold);
        }

        private void MacaoLabel_MouseLeave(object sender, EventArgs e)
        {
            MacaoLabel.Font = new Font("Sitka Display", 20);
        }

        private void BlackJackLabel_Click(object sender, EventArgs e)
        {
            BlackJack f = new BlackJack();
            f.Show();
            this.Close();
        }

        private void MacaoHTP_Label_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("Rules.txt");
            }
            catch
            {
                MessageBox.Show("I'm missing a file. Have you deleted it?");
            }
        }

        private void BackToMainMenu_Button_Click(object sender, EventArgs e)
        {
            MainMenuForm f = new MainMenuForm();
            f.Show();
            this.Close();
        }

        private void ReadThis_Label_Click(object sender, EventArgs e)
        {
            string message;

            message = "This is a gambling type of game and it can develop addiction.";
            MessageBox.Show(message);
        }

        private void BlackJackHTP_Label_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("BlackJackRules.txt");
            }
            catch
            {
                MessageBox.Show("I'm missing a file. Have you deleted it?");
            }
        }
    }
}
