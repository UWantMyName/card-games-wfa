﻿namespace CardGames
{
    partial class Macao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Macao));
            this.CardsLeft_Label = new System.Windows.Forms.Label();
            this.Deck_Picture = new System.Windows.Forms.PictureBox();
            this.EnemyMessage_Label = new System.Windows.Forms.Label();
            this.TopCard = new System.Windows.Forms.PictureBox();
            this.LastCardPlayed_Label = new System.Windows.Forms.Label();
            this.CommandPanel = new System.Windows.Forms.Panel();
            this.ChangeSignCode_Panel = new System.Windows.Forms.Panel();
            this.Diamonds = new System.Windows.Forms.PictureBox();
            this.Clubs = new System.Windows.Forms.PictureBox();
            this.Hearts = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Spades = new System.Windows.Forms.PictureBox();
            this.YourTurn_Label = new System.Windows.Forms.Label();
            this.DeleteLastCard_Button = new System.Windows.Forms.Button();
            this.InsertQueue_Button = new System.Windows.Forms.Button();
            this.QueueCards_Label = new System.Windows.Forms.Label();
            this.MyCards_ComboBox = new System.Windows.Forms.ComboBox();
            this.StandTurn_Button = new System.Windows.Forms.Button();
            this.PlayCards_Button = new System.Windows.Forms.Button();
            this.DrawAttackCards_Button = new System.Windows.Forms.Button();
            this.EndTurn_Button = new System.Windows.Forms.Button();
            this.Quit_Button = new System.Windows.Forms.Button();
            this.AceSignCode = new System.Windows.Forms.Label();
            this.EnemyNrCards_Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Deck_Picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopCard)).BeginInit();
            this.CommandPanel.SuspendLayout();
            this.ChangeSignCode_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Diamonds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clubs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hearts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Spades)).BeginInit();
            this.SuspendLayout();
            // 
            // CardsLeft_Label
            // 
            this.CardsLeft_Label.AutoSize = true;
            this.CardsLeft_Label.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.CardsLeft_Label.Location = new System.Drawing.Point(12, 288);
            this.CardsLeft_Label.Name = "CardsLeft_Label";
            this.CardsLeft_Label.Size = new System.Drawing.Size(105, 22);
            this.CardsLeft_Label.TabIndex = 1;
            this.CardsLeft_Label.Text = "Cards Left: ";
            // 
            // Deck_Picture
            // 
            this.Deck_Picture.Image = ((System.Drawing.Image)(resources.GetObject("Deck_Picture.Image")));
            this.Deck_Picture.Location = new System.Drawing.Point(12, 105);
            this.Deck_Picture.Name = "Deck_Picture";
            this.Deck_Picture.Size = new System.Drawing.Size(125, 180);
            this.Deck_Picture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Deck_Picture.TabIndex = 0;
            this.Deck_Picture.TabStop = false;
            // 
            // EnemyMessage_Label
            // 
            this.EnemyMessage_Label.AutoSize = true;
            this.EnemyMessage_Label.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.EnemyMessage_Label.Location = new System.Drawing.Point(340, 12);
            this.EnemyMessage_Label.Name = "EnemyMessage_Label";
            this.EnemyMessage_Label.Size = new System.Drawing.Size(98, 22);
            this.EnemyMessage_Label.TabIndex = 2;
            this.EnemyMessage_Label.Text = "Deciding...";
            // 
            // TopCard
            // 
            this.TopCard.Location = new System.Drawing.Point(415, 105);
            this.TopCard.Name = "TopCard";
            this.TopCard.Size = new System.Drawing.Size(125, 180);
            this.TopCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.TopCard.TabIndex = 3;
            this.TopCard.TabStop = false;
            // 
            // LastCardPlayed_Label
            // 
            this.LastCardPlayed_Label.AutoSize = true;
            this.LastCardPlayed_Label.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.LastCardPlayed_Label.Location = new System.Drawing.Point(171, 173);
            this.LastCardPlayed_Label.Name = "LastCardPlayed_Label";
            this.LastCardPlayed_Label.Size = new System.Drawing.Size(156, 22);
            this.LastCardPlayed_Label.TabIndex = 4;
            this.LastCardPlayed_Label.Text = "Last Cards Played";
            // 
            // CommandPanel
            // 
            this.CommandPanel.Controls.Add(this.ChangeSignCode_Panel);
            this.CommandPanel.Controls.Add(this.YourTurn_Label);
            this.CommandPanel.Controls.Add(this.DeleteLastCard_Button);
            this.CommandPanel.Controls.Add(this.InsertQueue_Button);
            this.CommandPanel.Controls.Add(this.QueueCards_Label);
            this.CommandPanel.Controls.Add(this.MyCards_ComboBox);
            this.CommandPanel.Controls.Add(this.StandTurn_Button);
            this.CommandPanel.Controls.Add(this.PlayCards_Button);
            this.CommandPanel.Controls.Add(this.DrawAttackCards_Button);
            this.CommandPanel.Controls.Add(this.EndTurn_Button);
            this.CommandPanel.Location = new System.Drawing.Point(13, 373);
            this.CommandPanel.Name = "CommandPanel";
            this.CommandPanel.Size = new System.Drawing.Size(775, 233);
            this.CommandPanel.TabIndex = 5;
            this.CommandPanel.Visible = false;
            // 
            // ChangeSignCode_Panel
            // 
            this.ChangeSignCode_Panel.Controls.Add(this.Diamonds);
            this.ChangeSignCode_Panel.Controls.Add(this.Clubs);
            this.ChangeSignCode_Panel.Controls.Add(this.Hearts);
            this.ChangeSignCode_Panel.Controls.Add(this.label1);
            this.ChangeSignCode_Panel.Controls.Add(this.Spades);
            this.ChangeSignCode_Panel.Location = new System.Drawing.Point(542, 3);
            this.ChangeSignCode_Panel.Name = "ChangeSignCode_Panel";
            this.ChangeSignCode_Panel.Size = new System.Drawing.Size(219, 223);
            this.ChangeSignCode_Panel.TabIndex = 7;
            this.ChangeSignCode_Panel.Visible = false;
            // 
            // Diamonds
            // 
            this.Diamonds.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Diamonds.Image = ((System.Drawing.Image)(resources.GetObject("Diamonds.Image")));
            this.Diamonds.Location = new System.Drawing.Point(123, 140);
            this.Diamonds.Name = "Diamonds";
            this.Diamonds.Size = new System.Drawing.Size(68, 65);
            this.Diamonds.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Diamonds.TabIndex = 14;
            this.Diamonds.TabStop = false;
            this.Diamonds.Click += new System.EventHandler(this.Diamonds_Click);
            // 
            // Clubs
            // 
            this.Clubs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Clubs.Image = ((System.Drawing.Image)(resources.GetObject("Clubs.Image")));
            this.Clubs.Location = new System.Drawing.Point(123, 42);
            this.Clubs.Name = "Clubs";
            this.Clubs.Size = new System.Drawing.Size(68, 65);
            this.Clubs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Clubs.TabIndex = 13;
            this.Clubs.TabStop = false;
            this.Clubs.Click += new System.EventHandler(this.Clubs_Click);
            // 
            // Hearts
            // 
            this.Hearts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Hearts.Image = ((System.Drawing.Image)(resources.GetObject("Hearts.Image")));
            this.Hearts.Location = new System.Drawing.Point(26, 140);
            this.Hearts.Name = "Hearts";
            this.Hearts.Size = new System.Drawing.Size(68, 65);
            this.Hearts.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Hearts.TabIndex = 12;
            this.Hearts.TabStop = false;
            this.Hearts.Click += new System.EventHandler(this.Hearts_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.label1.Location = new System.Drawing.Point(21, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 22);
            this.label1.TabIndex = 11;
            this.label1.Text = "Choose the next sign";
            // 
            // Spades
            // 
            this.Spades.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Spades.Image = ((System.Drawing.Image)(resources.GetObject("Spades.Image")));
            this.Spades.Location = new System.Drawing.Point(26, 42);
            this.Spades.Name = "Spades";
            this.Spades.Size = new System.Drawing.Size(68, 65);
            this.Spades.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Spades.TabIndex = 0;
            this.Spades.TabStop = false;
            this.Spades.Click += new System.EventHandler(this.Spades_Click);
            // 
            // YourTurn_Label
            // 
            this.YourTurn_Label.AutoSize = true;
            this.YourTurn_Label.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.YourTurn_Label.Location = new System.Drawing.Point(327, 0);
            this.YourTurn_Label.Name = "YourTurn_Label";
            this.YourTurn_Label.Size = new System.Drawing.Size(97, 22);
            this.YourTurn_Label.TabIndex = 7;
            this.YourTurn_Label.Text = "Your turn!";
            // 
            // DeleteLastCard_Button
            // 
            this.DeleteLastCard_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.DeleteLastCard_Button.Location = new System.Drawing.Point(198, 146);
            this.DeleteLastCard_Button.Name = "DeleteLastCard_Button";
            this.DeleteLastCard_Button.Size = new System.Drawing.Size(114, 23);
            this.DeleteLastCard_Button.TabIndex = 10;
            this.DeleteLastCard_Button.Text = "Undo";
            this.DeleteLastCard_Button.UseVisualStyleBackColor = true;
            this.DeleteLastCard_Button.Click += new System.EventHandler(this.DeleteLastCard_Button_Click);
            // 
            // InsertQueue_Button
            // 
            this.InsertQueue_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.InsertQueue_Button.Location = new System.Drawing.Point(198, 97);
            this.InsertQueue_Button.Name = "InsertQueue_Button";
            this.InsertQueue_Button.Size = new System.Drawing.Size(114, 23);
            this.InsertQueue_Button.TabIndex = 9;
            this.InsertQueue_Button.Text = "Place in queue";
            this.InsertQueue_Button.UseVisualStyleBackColor = true;
            this.InsertQueue_Button.Click += new System.EventHandler(this.InsertQueue_Button_Click);
            // 
            // QueueCards_Label
            // 
            this.QueueCards_Label.AutoSize = true;
            this.QueueCards_Label.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.QueueCards_Label.Location = new System.Drawing.Point(383, 98);
            this.QueueCards_Label.Name = "QueueCards_Label";
            this.QueueCards_Label.Size = new System.Drawing.Size(144, 22);
            this.QueueCards_Label.TabIndex = 8;
            this.QueueCards_Label.Text = "Play these cards:";
            // 
            // MyCards_ComboBox
            // 
            this.MyCards_ComboBox.FormattingEnabled = true;
            this.MyCards_ComboBox.Location = new System.Drawing.Point(18, 102);
            this.MyCards_ComboBox.Name = "MyCards_ComboBox";
            this.MyCards_ComboBox.Size = new System.Drawing.Size(121, 21);
            this.MyCards_ComboBox.TabIndex = 4;
            // 
            // StandTurn_Button
            // 
            this.StandTurn_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StandTurn_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.StandTurn_Button.Location = new System.Drawing.Point(418, 38);
            this.StandTurn_Button.Name = "StandTurn_Button";
            this.StandTurn_Button.Size = new System.Drawing.Size(96, 23);
            this.StandTurn_Button.TabIndex = 3;
            this.StandTurn_Button.Text = "Stand Turn";
            this.StandTurn_Button.UseVisualStyleBackColor = true;
            this.StandTurn_Button.Visible = false;
            this.StandTurn_Button.Click += new System.EventHandler(this.StandTurn_Button_Click);
            // 
            // PlayCards_Button
            // 
            this.PlayCards_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PlayCards_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.PlayCards_Button.Location = new System.Drawing.Point(318, 38);
            this.PlayCards_Button.Name = "PlayCards_Button";
            this.PlayCards_Button.Size = new System.Drawing.Size(75, 23);
            this.PlayCards_Button.TabIndex = 2;
            this.PlayCards_Button.Text = "Play Cards";
            this.PlayCards_Button.UseVisualStyleBackColor = true;
            this.PlayCards_Button.Visible = false;
            this.PlayCards_Button.Click += new System.EventHandler(this.PlayCards_Button_Click);
            // 
            // DrawAttackCards_Button
            // 
            this.DrawAttackCards_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DrawAttackCards_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.DrawAttackCards_Button.Location = new System.Drawing.Point(198, 38);
            this.DrawAttackCards_Button.Name = "DrawAttackCards_Button";
            this.DrawAttackCards_Button.Size = new System.Drawing.Size(94, 23);
            this.DrawAttackCards_Button.TabIndex = 1;
            this.DrawAttackCards_Button.Text = "Draw   Cards";
            this.DrawAttackCards_Button.UseVisualStyleBackColor = true;
            this.DrawAttackCards_Button.Visible = false;
            this.DrawAttackCards_Button.Click += new System.EventHandler(this.DrawAttackCards_Button_Click);
            // 
            // EndTurn_Button
            // 
            this.EndTurn_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.EndTurn_Button.Font = new System.Drawing.Font("Modern No. 20", 10F);
            this.EndTurn_Button.Location = new System.Drawing.Point(18, 38);
            this.EndTurn_Button.Name = "EndTurn_Button";
            this.EndTurn_Button.Size = new System.Drawing.Size(155, 23);
            this.EndTurn_Button.TabIndex = 0;
            this.EndTurn_Button.Text = "End Turn (Draw a card)";
            this.EndTurn_Button.UseVisualStyleBackColor = true;
            this.EndTurn_Button.Visible = false;
            this.EndTurn_Button.Click += new System.EventHandler(this.EndTurn_Button_Click);
            // 
            // Quit_Button
            // 
            this.Quit_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Quit_Button.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.Quit_Button.Location = new System.Drawing.Point(731, 12);
            this.Quit_Button.Name = "Quit_Button";
            this.Quit_Button.Size = new System.Drawing.Size(60, 35);
            this.Quit_Button.TabIndex = 6;
            this.Quit_Button.Text = "Quit";
            this.Quit_Button.UseVisualStyleBackColor = true;
            this.Quit_Button.Click += new System.EventHandler(this.Quit_Button_Click);
            // 
            // AceSignCode
            // 
            this.AceSignCode.AutoSize = true;
            this.AceSignCode.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.AceSignCode.Location = new System.Drawing.Point(560, 173);
            this.AceSignCode.Name = "AceSignCode";
            this.AceSignCode.Size = new System.Drawing.Size(189, 22);
            this.AceSignCode.TabIndex = 8;
            this.AceSignCode.Text = "Changed the sign into:";
            this.AceSignCode.Visible = false;
            // 
            // EnemyNrCards_Label
            // 
            this.EnemyNrCards_Label.AutoSize = true;
            this.EnemyNrCards_Label.Font = new System.Drawing.Font("Modern No. 20", 15F);
            this.EnemyNrCards_Label.Location = new System.Drawing.Point(12, 12);
            this.EnemyNrCards_Label.Name = "EnemyNrCards_Label";
            this.EnemyNrCards_Label.Size = new System.Drawing.Size(129, 22);
            this.EnemyNrCards_Label.TabIndex = 9;
            this.EnemyNrCards_Label.Text = "The enemy has";
            // 
            // Macao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 618);
            this.Controls.Add(this.EnemyNrCards_Label);
            this.Controls.Add(this.AceSignCode);
            this.Controls.Add(this.Quit_Button);
            this.Controls.Add(this.CommandPanel);
            this.Controls.Add(this.LastCardPlayed_Label);
            this.Controls.Add(this.TopCard);
            this.Controls.Add(this.EnemyMessage_Label);
            this.Controls.Add(this.CardsLeft_Label);
            this.Controls.Add(this.Deck_Picture);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Macao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Macao";
            ((System.ComponentModel.ISupportInitialize)(this.Deck_Picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopCard)).EndInit();
            this.CommandPanel.ResumeLayout(false);
            this.CommandPanel.PerformLayout();
            this.ChangeSignCode_Panel.ResumeLayout(false);
            this.ChangeSignCode_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Diamonds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clubs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Hearts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Spades)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Deck_Picture;
        private System.Windows.Forms.Label CardsLeft_Label;
        private System.Windows.Forms.Label EnemyMessage_Label;
        private System.Windows.Forms.PictureBox TopCard;
        private System.Windows.Forms.Label LastCardPlayed_Label;
        private System.Windows.Forms.Panel CommandPanel;
        private System.Windows.Forms.Button Quit_Button;
        private System.Windows.Forms.Button StandTurn_Button;
        private System.Windows.Forms.Button PlayCards_Button;
        private System.Windows.Forms.Button DrawAttackCards_Button;
        private System.Windows.Forms.Button EndTurn_Button;
        private System.Windows.Forms.ComboBox MyCards_ComboBox;
        private System.Windows.Forms.Label YourTurn_Label;
        private System.Windows.Forms.Button InsertQueue_Button;
        private System.Windows.Forms.Label QueueCards_Label;
        private System.Windows.Forms.Button DeleteLastCard_Button;
        private System.Windows.Forms.Panel ChangeSignCode_Panel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox Spades;
        private System.Windows.Forms.PictureBox Diamonds;
        private System.Windows.Forms.PictureBox Clubs;
        private System.Windows.Forms.PictureBox Hearts;
        private System.Windows.Forms.Label AceSignCode;
        private System.Windows.Forms.Label EnemyNrCards_Label;
    }
}