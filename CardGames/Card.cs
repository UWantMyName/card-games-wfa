﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CardGames
{
    public class Card
    {
        public string symbol;
        public int signcode;
        // 3 for hearts
        // 4 for diamonds
        // 5 for clubs
        // 6 for spades
        public Card[] CompatibleCards = new Card[31];
        public int nrCompatibleCards = 0;

        public string GetNameOfFile()
        {
            string name = "card";

            if (this.signcode == 3) name += "Hearts";
            else if (this.signcode == 4) name += "Diamonds";
            else if (this.signcode == 5) name += "Clubs";
            else name += "Spades";

            name += this.symbol;
            name += ".png";

            return name;
        }

        public string GetNameOfCurrentCard()
        {
            string name = this.symbol;
            name += " of ";
            if (this.signcode == 3) name += "hearts";
            else if (this.signcode == 4) name += "diamonds";
            else if (this.signcode == 5) name += "clubs";
            else name += "spades";

            return name;
        }

        public string GetNameOfCard(Card c)
        {
            string name = c.symbol;
            name += " of ";
            if (c.signcode == 3) name += "hearts";
            else if (c.signcode == 4) name += "diamonds";
            else if (c.signcode == 5) name += "clubs";
            else name += "spades";

            return name;
        }

        public bool CompatibleWith(Card c)
        {
            int i;

            if (this.symbol == "2" && (c.symbol == "2" || c.symbol == "7")) return true;
            if (this.symbol == "3" && (c.symbol == "3" || c.symbol == "7")) return true;


            for (i = 1; i <= nrCompatibleCards; i++)
                if (c.symbol == this.CompatibleCards[i].symbol && c.signcode == this.CompatibleCards[i].signcode) return true;
                else if (this.signcode == c.signcode) return true;
                else if (this.symbol == c.symbol) return true;
            return false;
        }

        public void CompatibilityCards()
        {
            int i;

            for (i = 1; i <= 30; i++)
                this.CompatibleCards[i] = new Card();

            if (this.symbol == "2" || this.symbol == "3")
            {
                nrCompatibleCards = 0;
                for (i = 1; i <= 4; i++)
                {
                    this.CompatibleCards[++nrCompatibleCards].symbol = "2";
                    this.CompatibleCards[nrCompatibleCards].signcode = i + 2;
                }
                for (i = 1; i <= 4; i++)
                {
                    this.CompatibleCards[++nrCompatibleCards].symbol = "3";
                    this.CompatibleCards[nrCompatibleCards].signcode = i + 2;
                }
                for (i = 1; i <= 4; i++)
                {
                    this.CompatibleCards[i].symbol = "7";
                    this.CompatibleCards[i].signcode = i + 2;
                }
            }
            if (this.symbol == "4")
            {
                nrCompatibleCards = 8;
                for (i = 1; i <= 4; i++)
                {
                    this.CompatibleCards[i].symbol = this.symbol;
                    this.CompatibleCards[i].signcode = i + 2;
                }
                for (i = 5; i <= nrCompatibleCards; i++)
                {
                    this.CompatibleCards[i].symbol = "7";
                    this.CompatibleCards[i].signcode = i - 2;
                }
            }
            else if (this.symbol == "7")
            {
                nrCompatibleCards = 0;
                for (i = 1; i <= 4; i++)
                {
                    this.CompatibleCards[++nrCompatibleCards].symbol = "2";
                    this.CompatibleCards[nrCompatibleCards].signcode = i + 2;
                }
                for (i = 1; i <= 4; i++)
                {
                    this.CompatibleCards[++nrCompatibleCards].symbol = "3";
                    this.CompatibleCards[nrCompatibleCards].signcode = i + 2;
                }
                for (i = 1; i <= 4; i++)
                {
                    this.CompatibleCards[++nrCompatibleCards].symbol = "4";
                    this.CompatibleCards[nrCompatibleCards].signcode = i + 2;
                }
                for (i = 1; i <= 4; i++)
                {
                    this.CompatibleCards[++nrCompatibleCards].symbol = "7";
                    this.CompatibleCards[nrCompatibleCards].signcode = i + 2;
                }

                this.CompatibleCards[++nrCompatibleCards].symbol = "5"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "6"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "8"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "9"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "10"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "J"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "Q"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "K"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "A"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
            }
            else if (this.symbol == "A")
            {
                nrCompatibleCards = 0;
                this.CompatibleCards[++nrCompatibleCards].symbol = "A"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "2"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "3"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "4"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "5"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "6"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "7"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "8"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "9"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "10"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "J"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "Q"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
                this.CompatibleCards[++nrCompatibleCards].symbol = "K"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode;
            }
            else
            {
                for (i = 1; i <= 4; i++)
                {
                    this.CompatibleCards[i].symbol = "2";
                    this.CompatibleCards[i].signcode = i + 2;
                }
                for (i = 5; i <= 8; i++)
                {
                    this.CompatibleCards[i].symbol = "3";
                    this.CompatibleCards[i].signcode = i - 2;
                }
                for (i = 9; i <= 12; i++)
                {
                    this.CompatibleCards[i].symbol = this.symbol;
                    this.CompatibleCards[i].signcode = i - 6;
                }
                nrCompatibleCards = 12;
                if (this.symbol != "A") { this.CompatibleCards[++nrCompatibleCards].symbol = "A"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "4") { this.CompatibleCards[++nrCompatibleCards].symbol = "4"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "5") { this.CompatibleCards[++nrCompatibleCards].symbol = "5"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "6") { this.CompatibleCards[++nrCompatibleCards].symbol = "6"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "7") { this.CompatibleCards[++nrCompatibleCards].symbol = "7"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "8") { this.CompatibleCards[++nrCompatibleCards].symbol = "8"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "9") { this.CompatibleCards[++nrCompatibleCards].symbol = "9"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "10") { this.CompatibleCards[++nrCompatibleCards].symbol = "10"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "J") { this.CompatibleCards[++nrCompatibleCards].symbol = "J"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "Q") { this.CompatibleCards[++nrCompatibleCards].symbol = "Q"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
                if (this.symbol != "K") { this.CompatibleCards[++nrCompatibleCards].symbol = "K"; this.CompatibleCards[nrCompatibleCards].signcode = this.signcode; }
            }
        }
    }
}
